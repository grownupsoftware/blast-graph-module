![Alt text](./resources/images/b-circle-trans-100.png)
# Blast-Graph

## Getting Started

Add following gradle dependencies:

```
 compile "co.gusl:blast-graph-module:1.0-SNAPSHOT"

```
### Configuring Server

```
 // create a new Blast Graph
 BlastGraph blastGraph = new BlastGraphImpl();

 // create the graph module with the graph
 GraphModule graphModule = new GraphModule(blastGraph);

 // configure the blast server with the graph module
 BlastServer blast = Blast.blast(new VertxEngine(), graphModule);

 // add server to blast graph
 blastGraph.setBlastServer(blast);

```

### Angular2 Project

Add following to package.json and then run npm install

```
 "blast-graph-angular2": ">=0.0.1"
```

**Example of Angular2 component using blast-graph**

Example below is an Angular2 component that connects to a Blast server, and attaches a local copy (varaible) to a server side collection. When the data on the serverchanges the local copy will be automatically updated.

Note: you need the [blast-angular2-graph](https://bitbucket.org/grownupsoftware/blast-angular2-graph) when developing Angular2 projects that use the graph module.


```
import {Component, OnInit} from '@angular/core';
import {GraphBlastService} from 'blast-graph-angular2/blast';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
    blastService: GraphBlastService;

    // the variable that will hold the local copy
    deals: any = [];

    constructor() {
        // Step 1 - connect to blast server
        this.blastService = new GraphBlastService('ws://127.0.0.1:8081/blast');

        // Step 2 - attach graph's collection 'deals' to local array
        this.blastService.attach('deals', this.deals);
    }
}    
```
And to render the local copy, here is an HTML snippet. When the data on the server changes, the 'deals' ngModel is automatically updated and the display (reacts) accordingly:

```
    <div *ngFor="let deal of deals" >
        <img src="{{deal.product.image}}" />
        <div>{{deal.product.name}}</div>
        <div>{{deal.product.description}}</div>
    </div>
```





# Data Graph - The Theory

As an example we will base our data model on customers and their orders. So in a class diagram, we could expect the following, where a customer may have many orders and each order many items.

![Alt text](./resources/images/class-diagram.png)

In a data graph this can be represented as:
![Alt text](./resources/images/hierarchy.svg)

Using graph terminlogy, each bubble is referred to as a 'vertex' and the link between bubbles, an 'edge'.

With Blast Graph you can traverse this hierarchy via a **path**, or **key**.

A key of "customers" would return all customer data.

A key of "customers/id:1" would return all the orders for a customer that has a unique key field called 'id' with a value of 1.

A key of 'customers/id:1/orders/id:3/items/id:7' would return the item with an 'id' of 7, which is part of order (id = 3) by customer (id = 1).

A key of 'customers/id:1/orders' would return all orders for a specific customer, but only the orders, where as a key of 'customers/id:1' would return all customer information plus a field of 'orders' that contains an array of all the customer's orders.

The **key** is used in the 'fetch' and 'attach' commands. 'Fetch' is a snapshot, where as 'attach' links that specific vertex, and its children, to a local copy and when the server data changes, and it matches the attached **key** then the local copy will receive the update. (Note: the blast graph client will automatically update the local copy)


## The Theory - Under The Hood!
### The Data Model
This section explores how blast-graph builds and uses the data graph.

Firstly, everything 'hangs' off a **root** vertex and then data is grouped under the term 'collections'. For example, 'customers' would be a 'collection' of customer data.

We create a collection in the following way, note, CustomerDO is a POJO, that contains the customer information. All fields from this class will be used as the customer's data, so it is possible,via the excludeFields, to specify fields on the POJO that you would like to ignore.

Note: The code examples are all server side!

```
       GraphCollection<CustomerDO, Long> customers = blastGraph.createCollection()
                .name("customers")
                .keyfield("id")
                .dataclass(CustomerDO.class)
                .excludeFields("password")
                .build();
                
```

We also need to create collections for 'orders' and 'items'.

The internal data graph will now look as follows:

![Alt text](./resources/images/top-level.svg)

A fairly flat strucrure! The next step is to create relationships between our collections. Assuming we created 3 collections (using the code above) and they are called 'customers','orders' and 'items'.
And the 'orders' collection has a field callled 'customerId' and 'items' has a field called 'orderId' i.e. a way that 'orders' know that it is for a specific customer and 'items' for a specific order.

We can create a relationship or **link** between the collections:

```
       orders.linkedTo(customers).using("customerId");
       items.linkedTo(orders).using("orderId");

```
Note: It is always a 'child' to 'parent' link!

At this stage they are deliberatly called 'links' and not, in graph terminology 'edges'. The reason is that at the moment there is no 'physical' link between the collections. This, link information, is stored in the root as part of the **schema**. 
![Alt text](./resources/images/links.svg)

When a customer, order and items are added to the collection, this link information, is used to create the 'edges' or relationships between the data entities.

We can use our 'graphCollections' from above to add data. It is also possible to add data directly to the graph without using a 'graph collection. Once the collections have been created and the links defined then data can be added via server-side code or by a client.

We will continue with server-side, and use the graphCollections to add one customer, order and an item.

```
customers.add(cust);
orders.add(ord);
items.add(itm);
```
In the code above, 'cust', 'ord' and 'itm' are instances of the relevant POJOs i.e. read from another database or manually created.

The Data Graph will now look like the following:

![Alt text](./resources/images/first-data.svg)

And after another insert:

![Alt text](./resources/images/more-data.svg)


When the **add** is used it does the following:

1. Creates a new vertex with the unique identifier as key
2. Creates an 'edge' between this vertex and its parent collection
3. Checks if there are any 'links' defined and if there are it creates an 'edge' between the relevant vertices.

So now the concept of a **path** or **key** should make some sense, so with a key of 'customers/id:1/orders/id:2/items/id:3' we can access a specific vertex or entity. Likewise a key of 'customers/id:1/orders' we can get a list of all orders for a customer.

It is worth pointing out that the following keys will return the same information:

- 'customers/id:1/orders/id:2/items/id:3'
- 'orders/id:2/items/id:3'
- 'items/id:3'

We are just using a different path to traverse to the relevant entity or vertex.

### Connected Clients and Attachments
Above has described the internal workings of the data model. Blast, also takes advantage of the graph to manage attachments.
There are a couple of system collections linked to the **Root**.

**Client Master**

When a client connects to the blast server (with the graph module installed) it will create a client vertex with an 'edge' to the client master.

**Client Attachments**

When an 'attach' command is received an attach vertex is created, this then has an edge to the relevant vertex and also an edge to the relevant clients vertex.

In the diagram below, 2 clients have connected and attached to a **path**.

![Alt text](./resources/images/attachments.svg)

In summary the following happens:

1. Client #1 connects - a new vertex is created with an edge to the client master
2. Client #1 performs an 'attach' with the key "customers/id:4" - a new attach vertex is created with an edge to the 'attachments' collection, the relevant client vertex, and the entity vertex for the customer where id=4
3. Client #2 connects and performs an 'attach' with a key 'items/id:6' - relevant vertices and edges are created as above.

### Data Change
When the entity for item id=4 is updated with the following command *update("items/id:4",new values)* then the following logic is applied:

1. The vertex for id=4 is updated
2. A check is made to see if there are any edges to 'attached vertices'. In this case there is an 'attach #2', so an update instruction is sent with the path and any changed fields. The path in this example is 'items/id:1'
3. Then there is a 'bubble up' through the tree, checking for any 'attached vertices' and extending the path.
4. When the 'bubble up' reaches the vertex for customer id=4 there is an attachment. The path is now 'customers/id:1/orders/id:5/items/id=4' and the updated values are sent to the attached client.
5. The client code (e.g. blast-graph-angular2') then uses the path to find the right 'object' to update in the local cache.




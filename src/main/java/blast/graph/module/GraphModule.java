/*
 * Grownup Software Limited.
 */
package blast.graph.module;

import blast.Controllable;
import blast.command.CommandModule;
import blast.exception.BlastException;
import blast.module.BlastModule;
import blast.server.BlastServer;
import blast.graph.core.BlastGraph;
import blast.graph.core.commands.GraphCommand;
import blast.graph.core.events.AddEvent;
import blast.graph.core.events.AttachEvent;
import blast.graph.core.events.AttachmentsEvent;
import blast.graph.core.events.DetachAllEvent;
import blast.graph.core.events.DetachEvent;
import blast.graph.core.events.FetchEvent;
import blast.graph.core.events.RemoveEvent;
import blast.graph.core.events.SchemaEvent;
import blast.graph.core.events.UpdateEvent;
import blast.log.BlastLogger;

/**
 *
 * @author grant
 */
public class GraphModule implements BlastModule, Controllable {

    private final BlastLogger logger = BlastLogger.createLogger();

    public static final String GRAPH_MODULE_ID = "co.gusl.graph";

    private GraphCommandHandler theGraphCommandHandler;

    private BlastGraph theBlastGraph;

    private static boolean configured = false;

    public GraphModule(BlastGraph blastGraph) {
        this.theBlastGraph = blastGraph;
    }

    @Override
    public void configure(BlastServer server) throws BlastException {
        if (!configured) {
            configured = true;

            logger.info("-- configure Graph Module --");

            theBlastGraph.setBlastServer(server);

            CommandModule commandModule = (CommandModule) server.requireModule(CommandModule.COMMAND_DECODER_MODULE_ID, CommandModule.class);

            commandModule.registerCommand("attach", AttachEvent.class, GraphCommand.class);
            commandModule.registerCommand("detach", DetachEvent.class, GraphCommand.class);
            commandModule.registerCommand("detachAll", DetachAllEvent.class, GraphCommand.class);
            commandModule.registerCommand("fetch", FetchEvent.class, GraphCommand.class);
            commandModule.registerCommand("add", AddEvent.class, GraphCommand.class);
            commandModule.registerCommand("update", UpdateEvent.class, GraphCommand.class);
            commandModule.registerCommand("remove", RemoveEvent.class, GraphCommand.class);
            commandModule.registerCommand("schema", SchemaEvent.class, GraphCommand.class);
            commandModule.registerCommand("attachments", AttachmentsEvent.class, GraphCommand.class);

            // Add Graph to the Eventbus
            theGraphCommandHandler = new GraphCommandHandler(theBlastGraph);
            server.registerEventListener(theGraphCommandHandler);
        }
    }

    public void setGraph(BlastGraph theBlastGraph) {
        this.theBlastGraph = theBlastGraph;
    }

    public BlastGraph getGraph() {
        return theBlastGraph;
    }

    @Override
    public String getModuleID() {
        return GRAPH_MODULE_ID;
    }

    @Override
    public void startup() throws BlastException {
    }

    @Override
    public void shutdown() {
    }

}

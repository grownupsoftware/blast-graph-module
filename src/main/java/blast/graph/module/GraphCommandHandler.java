package blast.graph.module;

import blast.client.BlastServerClient;
import blast.client.ClientClosedEvent;
import blast.client.ClientConnectingEvent;
import blast.eventbus.OnEvent;
import blast.exception.BlastException;
import blast.graph.core.BlastGraph;
import blast.graph.core.dataobjects.GraphMessageType;
import static blast.graph.core.dataobjects.GraphMessageType.GRAPH_FETCH_RESPONSE;
import static blast.graph.core.dataobjects.GraphMessageType.GRAPH_INITIAL_LOAD_RESPONSE;
import static blast.graph.core.dataobjects.GraphMessageType.GRAPH_SCHEMA_RESPONSE;
import blast.graph.core.dataobjects.GraphResponseMessage;
import blast.graph.core.utils.SchemaUtils;
import blast.graph.core.events.AddEvent;
import blast.graph.core.events.AttachEvent;
import blast.graph.core.events.AttachmentsEvent;
import blast.graph.core.events.DetachAllEvent;
import blast.graph.core.events.DetachEvent;
import blast.graph.core.events.FetchEvent;
import blast.graph.core.events.RemoveEvent;
import blast.graph.core.events.SchemaEvent;
import blast.graph.core.events.UpdateEvent;
import blast.log.BlastLogger;
import java.util.List;
import java.util.Map;

/**
 *
 * @author grant
 */
public class GraphCommandHandler {

    private final BlastLogger logger = BlastLogger.createLogger();
    private final BlastGraph theBlastGraph;

    public GraphCommandHandler(BlastGraph blastGraph) {
        this.theBlastGraph = blastGraph;
    }

    @OnEvent(order = 1)
    public void handleClosedClient(ClientClosedEvent event) {
        theBlastGraph.closeClient(event.getClient());
    }

    @OnEvent(order = 99)
    public void handleOpenClient(ClientConnectingEvent event) {
        logger.info("[{}] Client Connected from: {}", event.getClient().getClientID(), event.getClient().getRemoteAddress());
        theBlastGraph.createClient(event.getClient());
    }

    @OnEvent
    public void handleFetch(FetchEvent event) {
        logger.debug("[{}] Fetch From: {}",
                event.getClient().getClientID(),
                event.getCommandData().getKey());

        try {
            //Either Map<String,Object> or List<Map<String,Object>>
            Object initialLoad = theBlastGraph.fetch(event.getClient(),
                    event.getCommandData().getKey(),
                    event.getCommandData().getParameters());

            sendResponse(event.getClient(), new GraphResponseMessage(GRAPH_FETCH_RESPONSE,
                    event.getCommandData().getCorrelationId(),
                    event.getCommandData().getKey(),
                    initialLoad));

        } catch (BlastException ex) {
            sendErrorResponse(event.getClient(), event.getCommandData().getCorrelationId(), ex);
        }

    }

    @OnEvent
    public void handleAttach(AttachEvent event) {
        logger.debug("[{}] Attaching to id: {} key: {}",
                event.getClient().getClientID(),
                event.getCommandData().getAttachmentId(),
                event.getCommandData().getKey());
        try {
            //Either Map<String,Object> or List<Map<String,Object>>
            Object initialLoad = theBlastGraph.attachTo(event.getClient(),
                    event.getCommandData().getAttachmentId(),
                    event.getCommandData().getKey(),
                    event.getCommandData().getParameters());

            sendResponse(event.getClient(), new GraphResponseMessage(GRAPH_INITIAL_LOAD_RESPONSE,
                    event.getCommandData().getCorrelationId(),
                    event.getCommandData().getAttachmentId(),
                    event.getCommandData().getKey(),
                    initialLoad));

        } catch (BlastException ex) {
            sendErrorResponse(event.getClient(), event.getCommandData().getCorrelationId(), ex);
        }

    }

    @OnEvent
    public void handleDetachAll(DetachAllEvent event) {
        logger.debug("[{}] Detaching from ALL", event.getClient().getClientID());
        try {
            theBlastGraph.detachFromAll(event.getClient());

            sendResponse(event.getClient(), new GraphResponseMessage(GraphMessageType.GRAPH_DETACH_ALL_RESPONSE,
                    event.getCommandData().getCorrelationId()));

        } catch (BlastException ex) {
            sendErrorResponse(event.getClient(), event.getCommandData().getCorrelationId(), ex);
        }
    }

    @OnEvent
    public void handleDetach(DetachEvent event) {
        logger.debug("[{}] Detaching from id: {} key:{}",
                event.getClient().getClientID(),
                event.getCommandData().getAttachmentId(),
                event.getCommandData().getKey());

        try {
            theBlastGraph.detachFrom(event.getClient(), event.getCommandData().getAttachmentId());

            sendResponse(event.getClient(), new GraphResponseMessage(GraphMessageType.GRAPH_DETACH_RESPONSE,
                    event.getCommandData().getCorrelationId()));

        } catch (BlastException ex) {
            sendErrorResponse(event.getClient(), event.getCommandData().getCorrelationId(), ex);
        }
    }

    @OnEvent
    public void handleClientAttachments(AttachmentsEvent event) {
        logger.debug("[{}] Connected Client's Attachments", event.getClient().getClientID());
        try {
            List<Map<String, Object>> clientAttachments = theBlastGraph.getClientAttachments(event.getClient());

            sendResponse(event.getClient(), new GraphResponseMessage(GraphMessageType.GRAPH_CLIENT_ATTACHMENTS_RESPONSE,
                    event.getCommandData().getCorrelationId(),
                    event.getCommandData().getKey(),
                    clientAttachments));

        } catch (BlastException ex) {
            sendErrorResponse(event.getClient(), event.getCommandData().getCorrelationId(), ex);
        }
    }

    @OnEvent
    public void handleAdd(AddEvent event) {
        logger.debug("[{}] Adding: {}", event.getClient().getClientID(), event.getCommandData());
        try {
            theBlastGraph.add(event.getCommandData().getKey(), event.getCommandData().getData());

            SendOkResponse(event.getClient(), event.getCommandData().getCorrelationId());

        } catch (BlastException ex) {
            sendErrorResponse(event.getClient(), event.getCommandData().getCorrelationId(), ex);
        }
    }

    @OnEvent
    public void handleUpdate(UpdateEvent event) {
        logger.debug("[{}] Updating: {}", event.getClient().getClientID(), event.getCommandData());
        try {
            theBlastGraph.update(event.getCommandData().getKey(), event.getCommandData().getData());

            SendOkResponse(event.getClient(), event.getCommandData().getCorrelationId());

        } catch (BlastException ex) {
            sendErrorResponse(event.getClient(), event.getCommandData().getCorrelationId(), ex);
        }
    }

    @OnEvent
    public void handleRemove(RemoveEvent event) {
        logger.debug("[{}] Removing: {}", event.getClient().getClientID(), event.getCommandData());
        try {
            theBlastGraph.remove(event.getCommandData().getKey());

            SendOkResponse(event.getClient(), event.getCommandData().getCorrelationId());

        } catch (BlastException ex) {
            sendErrorResponse(event.getClient(), event.getCommandData().getCorrelationId(), ex);
        }
    }

    @OnEvent
    public void handleSchema(SchemaEvent event) {
        logger.debug("[{}] Schema", event.getClient().getClientID());
        try {
            Map<String, Object> dumpSchema = SchemaUtils.getSchema(theBlastGraph.getGraph());
            sendResponse(event.getClient(), new GraphResponseMessage(GRAPH_SCHEMA_RESPONSE, event.getCommandData().getCorrelationId(),
                    event.getCommandData().getKey(),
                    dumpSchema));
        } catch (BlastException ex) {
            sendErrorResponse(event.getClient(), event.getCommandData().getCorrelationId(), ex);
        }
    }

    private void sendResponse(BlastServerClient client, GraphResponseMessage graphMessage) throws BlastException {
        client.queueMessage(graphMessage);
    }

    private void SendOkResponse(BlastServerClient client, Long correlationId) throws BlastException {
        client.queueMessage(GraphResponseMessage.OkMessage(correlationId));
    }

    private void sendErrorResponse(BlastServerClient client, Long correlationId, BlastException exception) {
        logger.error("[{}] Exception", client.getClientID(), exception);
        try {
            client.queueMessage(GraphResponseMessage.FailMessage(correlationId, exception.getMessage()));
        } catch (BlastException e) {
            logger.error("Cannot send fail message", e);
        }
    }

}

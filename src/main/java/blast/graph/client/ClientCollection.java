package blast.graph.client;

import blast.graph.core.utils.ModificationWatcher;
import java.util.List;
import java.util.Map;

/**
 *
 * @author grant
 */
public class ClientCollection {

    private final boolean isList;
    private final String key;
    private final Map<String, Object> data;
    private final List<Map<String, Object>> listData;
    private final ModificationWatcher modificationWatcher;
    private final String attachmentId;

    public ClientCollection(String attachmentId, String key, Map<String, Object> data, ModificationWatcher modificationWatcher) {
        this.attachmentId = attachmentId;
        this.key = key;
        this.data = data;
        this.isList = false;
        this.listData = null;
        this.modificationWatcher = modificationWatcher;
    }

    public ClientCollection(String attachmentId, String key, List<Map<String, Object>> listData, ModificationWatcher modificationWatcher) {
        this.attachmentId = attachmentId;
        this.key = key;
        this.listData = listData;
        this.isList = true;
        this.data = null;
        this.modificationWatcher = modificationWatcher;
    }

    public boolean isList() {
        return isList;
    }

    public String getKey() {
        return key;
    }

    public Map<String, Object> getData() {
        return data;
    }

    public List<Map<String, Object>> getListData() {
        return listData;
    }

    public ModificationWatcher getModificationWatcher() {
        return modificationWatcher;
    }

    public String getAttachmentId() {
        return attachmentId;
    }

    @Override
    public String toString() {
        return "ClientCollection{" + "isList=" + isList + ", key=" + key + ", data=" + data + ", listData=" + listData + ", modificationWatcher=" + modificationWatcher + ", attachmentId=" + attachmentId + '}';
    }

}

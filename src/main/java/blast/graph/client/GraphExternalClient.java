package blast.graph.client;

import blast.exception.BlastException;
import blast.external.client.ExternalClient;
import blast.external.client.ExternalClientEventHandler;
import blast.external.client.WebSocketException;
import blast.external.client.WebSocketMessage;
import blast.graph.core.commands.GraphCommand;
import blast.graph.core.dataobjects.GraphResponseMessage;
import blast.graph.core.dataobjects.Instruction;
import blast.graph.core.dataobjects.Operation;
import blast.graph.core.dataobjects.PathDetails;
import blast.graph.core.dataobjects.PathParameters;
import blast.graph.core.utils.CollectionTraversor;
import static blast.graph.core.utils.GraphUtils.getObjectMapper;
import blast.graph.core.utils.ModificationWatcher;
import blast.json.ObjectMapperFactory;
import blast.log.BlastLogger;
import blast.utils.BlastUtils;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.MapDifference;
import com.google.common.collect.Maps;
import java.io.IOException;
import java.net.URI;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;

/**
 *
 * @author grant
 */
public class GraphExternalClient extends ExternalClient implements ExternalClientEventHandler {

    protected final BlastLogger logger = BlastLogger.createLogger();

    private final ObjectMapper theObjectMapper = ObjectMapperFactory.createObjectMapper();

    private static final TypeReference MAP_TYPE_REF = new com.fasterxml.jackson.core.type.TypeReference<HashMap<String, Object>>() {
    };

    private final Map<String, ClientCollection> theCollections = new HashMap<>();
    private final Map<Long, CompletableFuture<?>> theCorrelatedFutures = new ConcurrentHashMap<>();

    AtomicLong theCorrrelationCounter = new AtomicLong(1);

    public GraphExternalClient(URI url) {
        super(url);
        // set the client as the event handler as well
        eventHandler = this;
    }

    public GraphExternalClient(URI url, String protocol) {
        super(url, protocol);
    }

    public GraphExternalClient(URI url, String protocol, Map<String, String> extraHeaders) {
        super(url, protocol, extraHeaders);
    }

    public <T> CompletableFuture<Boolean> add(String collectionName, T entity) throws BlastException {
        CompletableFuture<Boolean> future = new CompletableFuture<>();
        long correlationId = theCorrrelationCounter.getAndIncrement();
        theCorrelatedFutures.put(correlationId, future);

        sendMessage("add", correlationId, collectionName, entity);
        return future;

    }

    public <T> CompletableFuture<Boolean> update(String key, T entity) throws BlastException {
        CompletableFuture<Boolean> future = new CompletableFuture<>();
        long correlationId = theCorrrelationCounter.getAndIncrement();
        theCorrelatedFutures.put(correlationId, future);

        sendMessage("update", correlationId, key, entity);
        return future;

    }

    public CompletableFuture<Boolean> remove(String key) throws BlastException {
        CompletableFuture<Boolean> future = new CompletableFuture<>();
        long correlationId = theCorrrelationCounter.getAndIncrement();
        theCorrelatedFutures.put(correlationId, future);

        sendMessage("remove", correlationId, key);
        return future;
    }

    public CompletableFuture<String> attach(String key, Map<String, Object> data) throws BlastException {
        return attach(key, data, null, null);
    }

    public CompletableFuture<String> attach(String key, Map<String, Object> data, PathParameters pathParameters) throws BlastException {
        return attach(key, data, pathParameters, null);
    }

    public CompletableFuture<String> attach(String key, Map<String, Object> data, ModificationWatcher modificationWatcher) throws BlastException {
        return attach(key, data, null, modificationWatcher);
    }

    public CompletableFuture<String> attach(String key, Map<String, Object> data, PathParameters pathParameters, ModificationWatcher modificationWatcher) throws BlastException {

        if (key.isEmpty()) {
            key = "root";
        } else {
            PathDetails[] pathDetails = PathDetails.splitPath(key);
            if (pathDetails[pathDetails.length - 1].getKeyField() == null) {
                throw new BlastException("Can only attach a collection i.e. [" + pathDetails[pathDetails.length - 1].getCollection() + "] to a type of 'list'");
            }
        }

        CompletableFuture<String> future = new CompletableFuture<>();
        long correlationId = theCorrrelationCounter.getAndIncrement();
        theCorrelatedFutures.put(correlationId, future);

        UUID randomUUID = UUID.randomUUID();
        String attachmentId = randomUUID.toString();

        theCollections.put(attachmentId, new ClientCollection(attachmentId, key, data, modificationWatcher));

        sendMessage("attach", correlationId, key, pathParameters, attachmentId);
        return future;
    }

    public CompletableFuture<String> attach(String key, List<Map<String, Object>> data) throws BlastException {

        return attach(key, data, null, null);
    }

    public CompletableFuture<String> attach(String key, List<Map<String, Object>> data, PathParameters pathParameters) throws BlastException {

        return attach(key, data, pathParameters, null);
    }

    public CompletableFuture<String> attach(String key, List<Map<String, Object>> data, ModificationWatcher modificationWatcher) throws BlastException {

        return attach(key, data, null, modificationWatcher);
    }

    public CompletableFuture<String> attach(String key, List<Map<String, Object>> data, PathParameters pathParameters, ModificationWatcher modificationWatcher) throws BlastException {
        if (key.isEmpty()) {
            throw new BlastException("Root can not be attached to a 'list'");
        }
        PathDetails[] pathDetails = PathDetails.splitPath(key);
        if (pathDetails[pathDetails.length - 1].getKeyField() != null) {
            throw new BlastException("The key's last value is keyField:keyValue - so cannot be attached to a list - use a hashmap for your collection");
        }

        CompletableFuture<String> future = new CompletableFuture<>();
        long correlationId = theCorrrelationCounter.getAndIncrement();
        theCorrelatedFutures.put(correlationId, future);

        UUID randomUUID = UUID.randomUUID();
        String attachmentId = randomUUID.toString();

        theCollections.put(attachmentId, new ClientCollection(attachmentId, key, data, modificationWatcher));

        sendMessage("attach", correlationId, key, pathParameters, null, randomUUID.toString());
        return future;
    }

    public CompletableFuture<Boolean> detach(String attachmentId) throws BlastException {
        CompletableFuture<Boolean> future = new CompletableFuture<>();
        long correlationId = theCorrrelationCounter.getAndIncrement();
        theCorrelatedFutures.put(correlationId, future);
        ClientCollection clientCollection = theCollections.get(attachmentId);
        theCollections.remove(attachmentId);
        sendMessage("detach", correlationId, clientCollection.getKey(), null, attachmentId);
        return future;

    }

    public CompletableFuture<Boolean> detachAll() throws BlastException {
        CompletableFuture<Boolean> future = new CompletableFuture<>();
        long correlationId = theCorrrelationCounter.getAndIncrement();
        theCorrelatedFutures.put(correlationId, future);
        theCollections.clear();
        sendMessage("detachAll", correlationId);
        return future;

    }

    public CompletableFuture<List<Map<String, Object>>> getAttachments() throws BlastException {
        CompletableFuture<List<Map<String, Object>>> future = new CompletableFuture<>();
        long correlationId = theCorrrelationCounter.getAndIncrement();
        theCorrelatedFutures.put(correlationId, future);
        sendMessage("attachments", correlationId);
        return future;
    }

    public CompletableFuture<Map<String, Object>> fetch(String key) throws BlastException {
        return fetch(key, null);
    }

    public CompletableFuture<Map<String, Object>> fetchRoot() throws BlastException {
        return fetch("", null);
    }

    public CompletableFuture<Map<String, Object>> fetch(String key, PathParameters pathParameters) throws BlastException {
        CompletableFuture<Map<String, Object>> future = new CompletableFuture<>();
        long correlationId = theCorrrelationCounter.getAndIncrement();

        theCorrelatedFutures.put(correlationId, future);

        sendMessage("fetch", correlationId, key, pathParameters);
        return future;
    }

    public CompletableFuture<List<Map<String, Object>>> fetchAsList(String key) throws BlastException {
        return fetchAsList(key, null);
    }

    public CompletableFuture<List<Map<String, Object>>> fetchAsList(String key, PathParameters pathParameters) throws BlastException {
        CompletableFuture<List<Map<String, Object>>> future = new CompletableFuture<>();
        long correlationId = theCorrrelationCounter.getAndIncrement();

        theCorrelatedFutures.put(correlationId, future);

        sendMessage("fetch", correlationId, key, pathParameters);
        return future;
    }

    public CompletableFuture<Map<String, Object>> getSchema() throws BlastException {
        CompletableFuture<Map<String, Object>> future = new CompletableFuture<>();
        long correlationId = theCorrrelationCounter.getAndIncrement();

        theCorrelatedFutures.put(correlationId, future);

        sendMessage("schema", correlationId);
        return future;
    }

    private void sendMessage(String command, Long correlationId) throws BlastException {
        sendMessage(command, correlationId, null, null, null, null);
    }

    private void sendMessage(String command, Long correlationId, String key) throws BlastException {
        sendMessage(command, correlationId, key, null, null, null);
    }

    private void sendMessage(String command, Long correlationId, String key, Object data) throws BlastException {
        sendMessage(command, correlationId, key, null, data, null);
    }

    private void sendMessage(String command, Long correlationId, String key, PathParameters parameters) throws BlastException {
        sendMessage(command, correlationId, key, parameters, null, null);
    }

    private void sendMessage(String command, Long correlationId, String key, PathParameters parameters, String attachmentId) throws BlastException {
        sendMessage(command, correlationId, key, parameters, null, attachmentId);
    }

    private void sendMessage(String command, Long correlationId, String key, PathParameters parameters, Object data, String attachmentId) throws BlastException {
        GraphCommand graphCommand = new GraphCommand();
        graphCommand.setCmd(command);
        graphCommand.setCorrelationId(correlationId);
        graphCommand.setKey(key);
        if (parameters != null) {
            graphCommand.setParameters(parameters);
        }
        if (data != null) {
            graphCommand.setData(getObjectMapper().convertValue(data, MAP_TYPE_REF));
        }

        if (attachmentId != null) {
            graphCommand.setAttachmentId(attachmentId);
        }

        String msg;
        try {
            msg = getObjectMapper().writeValueAsString(graphCommand);
            send(msg);
        } catch (JsonProcessingException ex) {
            throw new BlastException("Failed to send message", ex);
        }
    }

    @Override
    public void onOpen() {
    }

    @Override
    public void onMessage(WebSocketMessage message) {
        handleCommand(message);
    }

    @Override
    public void onClose() {
    }

    @Override
    public void onError(WebSocketException ex) {
        logger.error("Error ", ex);
    }

    @Override
    public void onLogMessage(String msg) {
    }

    private String buildPath(int startIndex, PathDetails[] pathDetails, boolean addLastKey) {
        StringBuilder builder = new StringBuilder();
        for (int x = startIndex; x < pathDetails.length; x++) {
            if (builder.length() > 0) {
                builder.append("/");
            }
            builder.append(pathDetails[x].getCollection());
            if (x < pathDetails.length - 1 || addLastKey) {
                builder.append("/");
                if (pathDetails[x].getKeyField() != null) {
                    builder.append(pathDetails[x].getKeyField()).append(":").append(pathDetails[x].getKeyValue());
                }
            }

        }
        return builder.toString();
    }

    public String calculateTruePath(String key, String path, Operation operation) throws BlastException {
        logger.debug("key: {} path[start]=>{}", key, path);

        PathDetails[] keyDetails = PathDetails.splitPath(key);

        PathDetails[] pathDetails = PathDetails.splitPath(path);

        String truePath = null;

        if (keyDetails[keyDetails.length - 1].isRoot()) {
            truePath = buildPath(0, pathDetails, operation == Operation.UPDATE);
        } else if (keyDetails[keyDetails.length - 1].getKeyField() == null) {
            // collection is an array
            for (int x = 0; x < pathDetails.length; x++) {
                // loop through until collection in path matches last collection in key
                if (pathDetails[x].getCollection().equals(keyDetails[keyDetails.length - 1].getCollection())) {
                    truePath = buildPath(x, pathDetails, operation == Operation.UPDATE);
                    break;
                }
            }
        } else {
            // collection is a map
            if (keyDetails.length == pathDetails.length) {
                truePath = buildPath(pathDetails.length - 1, pathDetails, operation == Operation.UPDATE);
            } else {
                truePath = buildPath(keyDetails.length, pathDetails, operation == Operation.UPDATE);
            }
        }

        logger.debug("[{}] to path=>{}", operation, truePath);

        return truePath;

    }

    private void handleGraphModify(GraphResponseMessage graphMessage) throws BlastException {
        logger.debug("Handling a collection update: {}", graphMessage);

        if (graphMessage.getInstruction() == null
                || (graphMessage.getInstruction().getOperation() != Operation.REMOVE
                && (graphMessage.getInstruction().getChanges() == null || graphMessage.getInstruction().getChanges().isEmpty())
                && graphMessage.getInstruction().getRecord() == null)) {
            logger.warn("No Instruction or no changes - doing nothing");
            return;
        }

        ClientCollection clientCollection = theCollections.get(graphMessage.getAttachmentId());
        if (clientCollection == null) {
            logger.warn("Cannot find collection for {}", graphMessage.getAttachmentId());
            logger.warn("Received message is ...  {}", graphMessage);
            BlastUtils.safeStream(theCollections.entrySet()).forEach(entry -> {
                logger.warn("found collection for: {}", entry.getKey());
            });
            //logger.warn("Current Collections: {}", prettyPrint(theCollections));
            return;
        }

        logger.debug("key: {} instruction: {}", graphMessage.getKey(), graphMessage.getInstruction());

        String path = calculateTruePath(graphMessage.getKey(),
                graphMessage.getInstruction().getPath(),
                graphMessage.getInstruction().getOperation());

        List<Map<String, Object>> parentList;
        Map<String, Object> record;
        switch (graphMessage.getInstruction().getOperation()) {
            case ADD:
                if (clientCollection.isList()) {
                    parentList = clientCollection.getListData();

                } else {
                    parentList = CollectionTraversor.findList(path, clientCollection.getData());
                }

                parentList.add(graphMessage.getInstruction().getRecord());
                if (clientCollection.getModificationWatcher() != null) {
                    clientCollection.getModificationWatcher().added(graphMessage.getInstruction().getRecord());
                }
                break;
            case UPDATE:
                if (clientCollection.isList()) {
                    record = CollectionTraversor.findRecord(path, clientCollection.getListData());
                    applyChangeToRecord(record, graphMessage.getInstruction());
                    if (clientCollection.getModificationWatcher() != null) {
                        clientCollection.getModificationWatcher().changed(record);
                    }

                } else {
                    if (path.isEmpty() || graphMessage.getKey().endsWith(path)) {
                        // if key =  markets/id:101/runners/id:103 and path = runners/id:103
                        // then data is not actually hierarchal
                        record = clientCollection.getData();
                    } else {
                        record = CollectionTraversor.findRecord(path, clientCollection.getData());
                    }
                    applyChangeToRecord(record, graphMessage.getInstruction());
                    if (clientCollection.getModificationWatcher() != null) {
                        clientCollection.getModificationWatcher().changed(record);
                    }

                }
                break;

            case REMOVE:
                PathDetails[] pathDetails = PathDetails.splitPath(graphMessage.getInstruction().getPath());

                String recordKey = pathDetails[pathDetails.length - 1].getCollection() + "/"
                        + pathDetails[pathDetails.length - 1].getKeyField() + ":"
                        + pathDetails[pathDetails.length - 1].getKeyValue();

                logger.debug("remove record recordKey: {}", recordKey);

                if (clientCollection.isList()) {
                    parentList = clientCollection.getListData();
                    record = CollectionTraversor.findRecord(recordKey, clientCollection.getListData());

                } else {
                    parentList = CollectionTraversor.findList(path, clientCollection.getData());
                    if (recordKey.isEmpty() || graphMessage.getKey().endsWith(recordKey)) {
                        record = clientCollection.getData();
                    } else {
                        record = CollectionTraversor.findRecord(recordKey, clientCollection.getData());
                    }
                }
                parentList.remove(record);

                if (clientCollection.getModificationWatcher() != null) {
                    clientCollection.getModificationWatcher().removed(record);
                }

                break;
            default:
                throw new AssertionError(graphMessage.getInstruction().getOperation().name());

        }

    }

//
    private void applyChangeToRecord(Map<String, Object> collection, Instruction instruction) {
        BlastUtils.safeStream(instruction.getChanges()).forEach(change -> {
            // name = name of field, value = new value
            collection.put((String) change.get("name"), change.get("value"));
        });
    }

    private GraphResponseMessage getGraphMessage(String value) throws BlastException {
        try {
            return theObjectMapper.readValue(value, GraphResponseMessage.class);
        } catch (IOException ex) {
            throw new BlastException("Failed to convert message", ex);
        }
    }

    private void handleCommand(WebSocketMessage message) {
        try {
            if (message.isBinary()) {
                // this will cause a loop
                //eventHandler.onMessage(message);
                return;
            }
            GraphResponseMessage graphMessage = getGraphMessage(message.getText());
            logger.debug("handle graphMessage: {}", graphMessage);

            // 1st pass - handle responses that don't have a future attached
            switch (graphMessage.getGraphMessageType()) {
                case GRAPH_UPDATE_RESPONSE:
                case GRAPH_ADD_RESPONSE:
                case GRAPH_REMOVE_RESPONSE:
                    handleGraphModify(graphMessage);
                    return;
                case GRAPH_INITIAL_LOAD_RESPONSE:
                case GRAPH_FETCH_RESPONSE:
                case GRAPH_SCHEMA_RESPONSE:
                case GRAPH_CLIENT_ATTACHMENTS_RESPONSE:
                case GRAPH_OK_RESPONSE:
                case GRAPH_FAIL_RESPONSE:
                case GRAPH_DETACH_RESPONSE:
                case GRAPH_DETACH_ALL_RESPONSE:
                    // do nothing
                    break;
                default:
                    throw new AssertionError(graphMessage.getGraphMessageType().name());
            }

            CompletableFuture future = theCorrelatedFutures.get(graphMessage.getCorrelationId());
            if (future == null) {
                BlastException blastException = new BlastException("Failed to find correlation id: " + graphMessage.getCorrelationId());
                eventHandler.onError(new WebSocketException("Failed to find correlation id", blastException));
                return;
            }

            // 2nd pass - need to complete all futures
            switch (graphMessage.getGraphMessageType()) {
                case GRAPH_DETACH_RESPONSE:
                case GRAPH_DETACH_ALL_RESPONSE:
                case GRAPH_OK_RESPONSE:
                    future.complete(true);
                    break;
                case GRAPH_FAIL_RESPONSE:
                    future.complete(false);
                    break;
                case GRAPH_INITIAL_LOAD_RESPONSE:
                    handleInitialLoad(future, graphMessage);
                    break;
                case GRAPH_CLIENT_ATTACHMENTS_RESPONSE:
                case GRAPH_SCHEMA_RESPONSE:
                case GRAPH_FETCH_RESPONSE:
                    future.complete(graphMessage.getData());
                    break;
                case GRAPH_ADD_RESPONSE:
                case GRAPH_UPDATE_RESPONSE:
                case GRAPH_REMOVE_RESPONSE:
                    // ignore - done in 1st pass
                    break;
                default:
                    throw new AssertionError(graphMessage.getGraphMessageType().name());
            }
        } catch (BlastException ex) {
            eventHandler.onError(new WebSocketException(ex.getMessage(), ex));
        }
    }

    private void handleInitialLoad(CompletableFuture future, GraphResponseMessage graphMessage) {
        ClientCollection clientCollection = theCollections.get(graphMessage.getAttachmentId());
        if (clientCollection == null) {
            future.completeExceptionally(new BlastException("Failed to find collection for key: " + graphMessage.getKey()));
            return;
        }

        if (clientCollection.isList()) {
            // as its initial load we can just add to collection
            if (graphMessage.getData() != null) {
                clientCollection.getListData().addAll((List<Map<String, Object>>) graphMessage.getData());
                future.complete(graphMessage.getAttachmentId());
//                future.complete(clientCollection.getListData());
            }

        } else {
            mergeMap(clientCollection.getData(), (Map<String, Object>) graphMessage.getData());
            future.complete(graphMessage.getAttachmentId());
//            future.complete(clientCollection.getData());
        }

    }

    private void mergeMap(Map<String, Object> baseMap, Map<String, Object> changedMap) {
        MapDifference<String, Object> mapDifference = Maps.difference(changedMap, baseMap);

        // new entries
        BlastUtils.safeStream(mapDifference.entriesOnlyOnLeft().entrySet()).forEach(entry -> {
            logger.debug("New Data - key: {} value: {}", entry.getKey(), entry.getValue());
            // add to map
            baseMap.put(entry.getKey(), entry.getValue());
        });

        // entries on current that are missing in changes
        BlastUtils.safeStream(mapDifference.entriesOnlyOnRight().entrySet()).forEach(entry -> {
            // at this stage do nothing
            logger.debug("Missing Data in Changes -  key: {} value: {}", entry.getKey(), entry.getValue());
        });

        BlastUtils.safeStream(mapDifference.entriesDiffering().entrySet()).forEach(entry -> {
            logger.debug("Data changed - key: {} value: {}", entry.getKey(), entry.getValue());
            // overwrite value
            baseMap.put(entry.getKey(), entry.getValue());
        });

    }

}

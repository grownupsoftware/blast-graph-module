/*
 * Grownup Software Limited.
 */
package blast.graph.core;

import blast.client.BlastServerClient;
import blast.doc.DocField;
import blast.exception.BlastException;
import blast.graph.core.utils.GraphUtils;
import blast.graph.core.utils.GraphDataExtractor;
import blast.graph.core.dataobjects.Instruction;
import blast.graph.core.dataobjects.PathParameters;
import blast.graph.core.builders.CollectionBuilder;
import blast.server.BlastServer;
import static blast.graph.core.GraphCollection.COLLECTION_ATTACHMENT_EDGE;
import static blast.graph.core.GraphCollection.COLLECTION_CLIENT_ATTACHMENT_EDGE;
import static blast.graph.core.GraphCollection.COLLECTION_DATA_EDGE;
import static blast.graph.core.GraphCollection.COLLECTION_DATA_FIELD_NAMES;
import static blast.graph.core.GraphCollection.COLLECTION_EDGE_NAME;
import static blast.graph.core.GraphCollection.COLLECTION_FIELD_CHILD_RELATIONSHIPS;
import static blast.graph.core.GraphCollection.COLLECTION_FIELD_CLASSNAME;
import static blast.graph.core.GraphCollection.COLLECTION_FIELD_KEYNAME;
import static blast.graph.core.GraphCollection.COLLECTION_FIELD_KEYTYPE;
import static blast.graph.core.GraphCollection.COLLECTION_FIELD_NAME;
import static blast.graph.core.GraphCollection.COLLECTION_FIELD_PARENT_RELATIONSHIPS;
import static blast.graph.core.GraphCollection.COLLECTION_MASTER;
import static blast.graph.core.GraphCollection.COLLECTION_PREFIX;
import blast.graph.core.dataobjects.Operation;
import blast.graph.core.dataobjects.PathDetails;
import blast.log.BlastLogger;
import blast.utils.BlastUtils;
import com.tinkerpop.blueprints.Direction;
import com.tinkerpop.blueprints.Edge;
import com.tinkerpop.blueprints.Graph;
import com.tinkerpop.blueprints.GraphFactory;
import com.tinkerpop.blueprints.KeyIndexableGraph;
import com.tinkerpop.blueprints.Vertex;
import com.tinkerpop.blueprints.util.wrappers.id.IdGraph;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import org.apache.commons.configuration.BaseConfiguration;
import org.apache.commons.configuration.Configuration;

/**
 *
 * @author grant
 */
public class BlastGraphImpl implements BlastGraph {

    protected static final BlastLogger logger = BlastLogger.createLogger();

    @DocField(description = "The main graph instance")
    private final Graph theGraph;

    @DocField(description = "The client master collection")
    private Vertex theClientMaster;

    @DocField(description = "The attach master collection")
    private Vertex theAttachMaster;

    @DocField(description = "The associated blast server")
    private BlastServer theBlastServer;

    private final boolean useNeo4J = false;
    private final boolean audit = true;

    public BlastGraphImpl() {
        Configuration config = new BaseConfiguration();
        // default - in memory
        if (!useNeo4J) {
            config.setProperty("blueprints.graph", "com.tinkerpop.blueprints.impls.tg.TinkerGraph");
            // if want file system persistence
            // config.setProperty("blueprints.tg.directory", "/tmp/data");
            // config.setProperty("blueprints.tg.file-type", "GRAPHSON");
        } else {
            // neo4j
            config.setProperty("blueprints.graph", "com.tinkerpop.blueprints.impls.neo4j.Neo4jGraph");
            config.setProperty("blueprints.neo4j.directory", "/tmp/neo4j");
            config.setProperty("blueprints.neo4j.conf.neostore.nodestore.db.mapped_memory", "285M");
            config.setProperty("blueprints.neo4j.conf.neostore.relationshipstore.db.mapped_memory", "285M");
        }
        KeyIndexableGraph graph = (KeyIndexableGraph) GraphFactory.open(config);

        theGraph = new IdGraph(graph, true, false);

        initialise();

        //logger.info("Features: {}", theGraph.getFeatures());
    }

    private void initialise() {
        createClientAndAttachCollections();
    }

    /**
     * Create a 'client' collection - that stores Ids of all clients that have
     * an attachment.
     *
     * Create an 'attach-master' collection - when a vertex has an associated
     * attachment it will 1) create a new vertex ('attach') attached to the main
     * 'attach-master' collection 2) will create a client vertex if it does not
     * exist 3) will link the 'attach' vertex to the 'client' vertex so we end
     * up with:
     *
     * clients 1-* client 1-* attach *-1 attach-master
     *
     * so we can get all clients from 'clients, all client attachments
     *
     *
     */
    private void createClientAndAttachCollections() {

        Vertex master = getCollectionMaster();

        // possible in persisted environment the collections may already be set up
        // create client master
        if (!GraphUtils.collectionEdgeExists(master, COLLECTION_EDGE_NAME, CLIENT_MASTER)) {
            theClientMaster = theGraph.addVertex(COLLECTION_PREFIX + CLIENT_MASTER);
            theClientMaster.setProperty(COLLECTION_FIELD_NAME, CLIENT_MASTER);

            theGraph.addEdge(null, master, theClientMaster, COLLECTION_EDGE_NAME);
        }

        // create attach master
        if (!GraphUtils.collectionEdgeExists(master, COLLECTION_EDGE_NAME, ATTACH_MASTER)) {
            theAttachMaster = theGraph.addVertex(COLLECTION_PREFIX + ATTACH_MASTER);
            theAttachMaster.setProperty(COLLECTION_FIELD_NAME, ATTACH_MASTER);

            theGraph.addEdge(null, master, theAttachMaster, COLLECTION_EDGE_NAME);
        }

        // create audit master
        if (!GraphUtils.collectionEdgeExists(master, COLLECTION_EDGE_NAME, AUDIT_MASTER)) {
            Vertex child = theGraph.addVertex(COLLECTION_PREFIX + AUDIT_MASTER);
            child.setProperty(COLLECTION_FIELD_NAME, AUDIT_MASTER);

            theGraph.addEdge(null, master, child, COLLECTION_EDGE_NAME);
        }

    }

    /**
     * Gets the underlying Tinkerpop graph
     *
     * @return
     */
    @Override
    public Graph getGraph() {
        return theGraph;
    }

    /**
     * Gets root data
     *
     * @param params apply path parameters
     * @return map by collection of all collections
     * @throws BlastException
     */
    @Override
    public Map<String, Object> getRootData(PathParameters params) throws BlastException {
        return GraphDataExtractor.getData(theGraph, COLLECTION_MASTER, false, params);
    }

    /**
     * Get all data for a collection
     *
     * @param collectionName the name of the col
     * @param params apply path parameters
     * @return all entities for this collection
     * @throws BlastException
     */
    @Override
    public List<Map<String, Object>> getCollectionData(String collectionName, PathParameters params) throws BlastException {
        return GraphDataExtractor.getCollectionData(theGraph, collectionName, params);
    }

    /**
     * Gets data for a specific key - not collection related
     *
     * @param <K> The type of Key e.g. String, Long, Integer
     * @param key the value of the key
     * @return the data for this entity
     * @throws BlastException
     */
    @Override
    public <K> Map<String, Object> getData(K key, PathParameters params) throws BlastException {
        return GraphDataExtractor.getData(theGraph, key, false, params);
    }

    /**
     * Removes a collection
     *
     * @param collectionName The name of the collection to remove - also removes
     * all underling data
     */
    @Override
    public void removeCollection(String collectionName) {
        Vertex master = getCollectionMaster();
        Iterable<Edge> edges = master.getEdges(Direction.OUT, COLLECTION_EDGE_NAME);
        for (Edge edge : edges) {
            if (collectionName.equals(edge.getProperty(COLLECTION_FIELD_NAME))) {
                Vertex child = edge.getVertex(Direction.IN);
                theGraph.removeVertex(child);
            }
        }
    }

    /**
     * Schema dump of collections and their settings - no collection data is
     * returned
     *
     * @return list of graph collection summary
     * @throws BlastException
     */
    @Override
    public List<GraphCollection> getCollections() throws BlastException {
        List<GraphCollection> collections = new ArrayList<>();

        Vertex master = getCollectionMaster();

        List<String> internalCollections = BlastUtils.safeStream(INTERNAL_COLLECTIONS).map(name -> {
//            return COLLECTION_PREFIX + name;
            return name;
        }).collect(Collectors.toList());

        Iterable<Edge> edges = master.getEdges(Direction.OUT, COLLECTION_EDGE_NAME);
        for (Edge edge : edges) {

            Vertex child = edge.getVertex(Direction.IN);

            String name = child.getProperty(COLLECTION_FIELD_NAME);

            if (!internalCollections.contains(name)) {

                String classname = child.getProperty(COLLECTION_FIELD_CLASSNAME);
                if (classname == null) {
                    throw new BlastException("Associated data class is null");
                }
                Class<?> clazz;
                try {
                    clazz = Class.forName(classname);
                } catch (ClassNotFoundException ex) {
                    throw new BlastException("Failed to find class: " + classname, ex);
                }

                String keyType = child.getProperty(COLLECTION_FIELD_KEYTYPE);
                if (keyType == null) {
                    throw new BlastException("Associated data class is null");
                }
                Class<?> keyClass;
                try {
                    keyClass = Class.forName(keyType);
                } catch (ClassNotFoundException ex) {
                    throw new BlastException("Failed to find class: " + classname, ex);
                }

                String keyname = child.getProperty(COLLECTION_FIELD_KEYNAME);

                Map<String, Object> parentRelationships = GraphUtils.jsonToMap(child.getProperty(COLLECTION_FIELD_PARENT_RELATIONSHIPS));
                Map<String, Object> childRelationships = GraphUtils.jsonToMap(child.getProperty(COLLECTION_FIELD_CHILD_RELATIONSHIPS));

                GraphCollection graphCollection = new GraphCollection(theBlastServer, theGraph, child, name, keyname, clazz, keyClass, parentRelationships, childRelationships);
                Set<String> fieldnames = child.getProperty(COLLECTION_DATA_FIELD_NAMES);
                graphCollection.initialiseWithFields(child, fieldnames.toArray(new String[fieldnames.size()]));

                collections.add(graphCollection);
            }
        }

        return collections;

    }

    /**
     * Gets the collection master - all collections are connected to the master
     *
     * @return
     */
    private Vertex getCollectionMaster() {
        Vertex master = theGraph.getVertex(COLLECTION_MASTER);
        if (master == null) {
            master = theGraph.addVertex(COLLECTION_MASTER);
        }
        return master;
    }

    /**
     * Returns a builder for collection creation
     *
     * @return
     */
    public CollectionBuilder createCollection() {
        return new CollectionBuilder(this);
    }

    /**
     * Creates the collection
     *
     * @param <T> Type of underlying data class
     * @param <K> Type of key e.g. String, Long, Integer
     * @param collectionName The name of the collection to create
     * @param keyname The key name i.e. the name of the unique identifier -
     * normally 'id'
     * @param clazz The Data Class i.e. a POJO that is associated with this
     * collection
     * @param keyType The class of the key e.g. String,Long,Integer
     * @param includeFields only include fields when building the collection
     * @param excludeFields include all fields from POJO except these
     * @return A new Graph Collection
     * @throws BlastException
     */
    @Override
    public <T, K> GraphCollection createCollection(String collectionName, String keyname, Class<T> clazz, Class<K> keyType, String[] includeFields, String[] excludeFields) throws BlastException {
        // We store collections as a vertex, so that if we are using a persisted graph they are always available
        Vertex master = getCollectionMaster();
        if (GraphUtils.collectionEdgeExists(master, COLLECTION_EDGE_NAME, collectionName)) {
            throw new BlastException("Collection name already registered");
        }

        Vertex child = theGraph.addVertex(COLLECTION_PREFIX + collectionName);
        child.setProperty(COLLECTION_FIELD_NAME, collectionName);
        child.setProperty(COLLECTION_FIELD_KEYNAME, keyname);
        child.setProperty(COLLECTION_FIELD_CLASSNAME, clazz.getCanonicalName());
        child.setProperty(COLLECTION_FIELD_KEYTYPE, keyType.getCanonicalName());

        theGraph.addEdge(null, master, child, COLLECTION_EDGE_NAME);

        GraphCollection<T, K> graphCollection = new GraphCollection(theBlastServer, theGraph, child, collectionName, keyname, clazz, keyType);
        if (includeFields != null && includeFields.length > 0) {
            graphCollection.initialiseWithFields(child, includeFields);
        } else {
            graphCollection.initialiseFromClass(child, excludeFields);
        }
        return graphCollection;
    }

    /**
     * Get the data for an entity but only include children specified by the
     * 'includeRelationships'
     *
     * @param <K> The Type of key
     * @param key The value of the Key
     * @param includeRelationships An array of relationships (edges) to be
     * returned
     * @return Map of the data for this entity
     * @throws BlastException
     */
    @Override
    public <K> Map<String, Object> get(K key, String... includeRelationships) throws BlastException {
        Vertex vertex = theGraph.getVertex(key);
        if (vertex == null) {
            throw new BlastException("not found");
        }
        Map<String, Object> map = GraphUtils.convertToMap(vertex);

        BlastUtils.safeStream(includeRelationships).forEach(relationshipName -> {

            List<Map<String, Object>> list = new ArrayList<>();
            Iterable<Edge> edges = vertex.getEdges(Direction.OUT, relationshipName);
            edges.forEach(edge -> {
                list.add(GraphUtils.convertToMap(edge.getVertex(Direction.IN)));
            });
            map.put(relationshipName, list);
        });
        return map;
    }

//    /**
//     * Remove an entity
//     *
//     * @param <K> The Type of key
//     * @param key The value of the Key
//     */
//    @Override
//    public <K> void remove(K key) {
//        Vertex vertex = theGraph.getVertex(key);
//        if (vertex != null) {
//            theGraph.removeVertex(vertex);
//        }
//    }
    /**
     * Starting from a entity id bubble up through all parent relationships
     * (edges) until the parent collection is reached
     *
     * @param <K> The Type of key
     * @param key The value of the Key
     * @throws BlastException
     */
    @Override
    public <K> void bubbleUp(K key) throws BlastException {
        Vertex vertex = theGraph.getVertex(key);
        if (vertex == null) {
            throw new BlastException("No record found: " + key.toString());
        }
        List<Instruction> listInstructions = new ArrayList<>();

        bubbleUp(null, null, vertex, Operation.UPDATE, listInstructions);

        logger.info("instructions: {}", listInstructions);

    }

    /**
     * recursive bubbling up creating a list of instructions (add, remove,
     * update)
     *
     * @param path
     * @param collectionName
     * @param vertex The current vertex
     * @param operation The Operation Add, Remove, Update
     * @param listInstructions list of instructions (operations) to add to
     */
    private void bubbleUp(String path, String collectionName, Vertex vertex, Operation operation, List<Instruction> listInstructions) {
        logger.debug("bubbleup - touched: {}", vertex.getId());
        Instruction instruction = new Instruction(operation);
        if (path == null) {
            path = "";
        }
        path = "id" + ":" + vertex.getId().toString() + (collectionName != null ? "/" + collectionName + "/" : "") + path;
        instruction.setPath(path);
        listInstructions.add(instruction);

        Iterable<Edge> edges = vertex.getEdges(Direction.IN);

        for (Edge edge : edges) {
            switch (edge.getLabel()) {
                case COLLECTION_EDGE_NAME:
                case COLLECTION_DATA_EDGE:
                    // ignore - not interested in this data
                    break;
                default:
                    Vertex parent = edge.getVertex(Direction.OUT);
                    logger.debug("bubble up edge: {} parent: {}", edge.getLabel(), parent.getId());
                    bubbleUp(path, edge.getLabel(), parent, operation, listInstructions);
            }
        }
    }

    /**
     *
     * @param client The blast server client
     * @param key follows format of "collection" or
     * "collection/keyfield:keyvalue" or
     * "collection/keyfield:keyvalue/childcollection"
     * @param params additional parameters
     * @return Either Map<String,Object> or List<Map<String,Object>>
     * @throws BlastException
     */
    @Override
    public Object fetch(BlastServerClient client, String key, PathParameters params) throws BlastException {

        if (audit) {
            audit("fetch", client, key, params);
        }
        return GraphDataExtractor.fetch(theBlastServer, theGraph, key, params);
    }

    /**
     * Gets the client vertex linked to Blasts id
     *
     * @param client
     * @return
     */
    private Vertex createClientVertex(BlastServerClient client) {
        Vertex clientVertex = theGraph.getVertex(client.getClientID());
        if (clientVertex == null) {
            clientVertex = theGraph.addVertex(client.getClientID());
            clientVertex.setProperty("remote-address", client.getRemoteAddress());
            clientVertex.setProperty("start-time", LocalDateTime.now());

//            theGraph.addEdge(null, theClientMaster, clientVertex, CLIENT_MASTER_EDGE);
            theGraph.addEdge(null, theClientMaster, clientVertex, COLLECTION_DATA_EDGE);

        }
        return clientVertex;
    }

    private Vertex createAttachVertex(Vertex clientVertex, String attachmentId, String key, PathParameters params) throws BlastException {
        String attachId = clientVertex.getId() + "-" + attachmentId;

        Vertex attachVertex;

        attachVertex = theGraph.getVertex(attachId);
        if (attachVertex != null) {
            return attachVertex;
        }
        // don't have an attachment so create one
        attachVertex = theGraph.addVertex(clientVertex.getId() + "-" + attachmentId);

        attachVertex.setProperty("client-id", clientVertex.getId());
        attachVertex.setProperty("start-time", LocalDateTime.now());
        attachVertex.setProperty("key", key);
        attachVertex.setProperty("attachmentId", attachmentId);

        if (params == null) {
            attachVertex.setProperty("have-params", false);
        } else {
            // add PathParameters to edge
            attachVertex.setProperty("have-params", true);

            attachVertex.setProperty("include-children", params.hasIncludeChildren());

            if (params.getFields() != null && !params.getFields().isEmpty()) {
                attachVertex.setProperty("include-fields", GraphUtils.objectToJson(params.getFields()));
            }
            if (params.getPredicates() != null && !params.getPredicates().isEmpty()) {
                attachVertex.setProperty("predicates", GraphUtils.objectToJson(params.getPredicates()));
            }
        }

        // link to attach master
        theGraph.addEdge(null, theAttachMaster, attachVertex, COLLECTION_DATA_EDGE);
        // link to client
        theGraph.addEdge(null, clientVertex, attachVertex, COLLECTION_CLIENT_ATTACHMENT_EDGE);

        return attachVertex;
    }

    private void audit(String command, BlastServerClient client, String key, PathParameters params) {
        logger.info("AUDIT client: {} command: {} key: {} params: {}", client.getClientID(), command, key, params);
    }

    private Vertex getVertex(String collection, String keyValue) throws BlastException {
        GraphCollection graphCollection = GraphCollection.getCollection(theBlastServer, theGraph, collection);
        Object convertedKeyValue = GraphUtils.getKeyValue(graphCollection.getKeyType(), keyValue);
        return theGraph.getVertex(convertedKeyValue);
    }

    private void performAttachment(Vertex attachVertex, String attachmentId, String key, PathParameters params) throws BlastException {
        PathDetails[] pathDetails = PathDetails.splitPath(key);
        Vertex recordVertex;

        if (pathDetails.length == 1) {
            // simple path - can only be 'root' or a 'collection'
            if (pathDetails[0].isRoot()) {
                recordVertex = getCollectionMaster();
            } else if (pathDetails[0].isCollectionOnly()) {
                recordVertex = theGraph.getVertex(COLLECTION_PREFIX + pathDetails[0].getCollection());
            } else {
                if (pathDetails[0].getCollection().equals(CLIENT_MASTER) || pathDetails[0].getCollection().equals(ATTACH_MASTER)) {
                    // these are system collections
                    recordVertex = theGraph.getVertex(pathDetails[0].getKeyValue());
                } else {

                    if (pathDetails[0].getCollection() != null && pathDetails[0].getKeyField() != null) {
                        recordVertex = getVertex(pathDetails[0].getCollection(), pathDetails[0].getKeyValue());
                    } else {
                        // should not get here - should be caught earlier
                        throw new BlastException("Must specify a collection");
                    }
                }
            }
        } else {
            // complex path - can be a 'collection' of a parent, or parent
            int index = pathDetails.length - 1;
            if (pathDetails[index].getKeyValue() != null) {
                // has a key, as keys are unique we can go and get data straight away
                recordVertex = getVertex(pathDetails[index].getCollection(), pathDetails[index].getKeyValue());
            } else {
                // so it is a collection of a parent id e.g events/id:100/markets
                // need to get collection from parent
                recordVertex = getVertex(pathDetails[0].getCollection(), pathDetails[index - 1].getKeyValue());
            }
        }

        // do the attachment
        Edge edge = theGraph.addEdge(null, recordVertex, attachVertex, COLLECTION_ATTACHMENT_EDGE);
        edge.setProperty("attachmentId", attachmentId);
        edge.setProperty("key", key);

    }

    @Override
    public void closeClient(BlastServerClient client) {
        Vertex clientVertex = theGraph.getVertex(client.getClientID());
        if (clientVertex != null) {

            // remove all attachments - client->attachment link will auto delete - we want to delete the attachment->record reference    
            Iterable<Edge> edges = clientVertex.getEdges(Direction.OUT, COLLECTION_CLIENT_ATTACHMENT_EDGE); //, COLLECTION_ATTACHMENT_EDGE
            for (Edge edge : edges) {
                Vertex attachVertex = edge.getVertex(Direction.IN);
                String attachmentId = (String) attachVertex.getProperty("attachmentId");
                String key = (String) attachVertex.getProperty("key");
                logger.info("[{}] Detach from id: [{}] key: {}", client.getClientID(), attachmentId, key);
                theGraph.removeVertex(attachVertex);
            }

            Object time = clientVertex.getProperty("start-time");
            if (time != null) {
                logger.info("[{}] Bye! - nice knowing you for: {}", client.getClientID(), GraphUtils.prettyTimeDifference((LocalDateTime) time, LocalDateTime.now()));
            }

            // finally remove the client
            theGraph.removeVertex(clientVertex);
        }
    }

    @Override
    public void createClient(BlastServerClient client) {
        createClientVertex(client);
    }

    /**
     *
     * @param client the blast server client
     * @param key follows format of "collection" or
     * "collection/keyfield:keyvalue" or
     * "collection/keyfield:keyvalue/childcollection"
     * @param attachmentId client side unique identifier for this collection
     * @param params additional parameters
     * @return Either Map<String,Object> or List<Map<String,Object>>
     * @throws BlastException
     */
    @Override
    public Object attachTo(BlastServerClient client, String attachmentId, String key, PathParameters params) throws BlastException {

        if (key == null || key.isEmpty()) {
            key = "root";
        }
        Vertex clientVertex = createClientVertex(client);
        Vertex attachVertex = createAttachVertex(clientVertex, attachmentId, key, params);
        performAttachment(attachVertex, attachmentId, key, params);

        if (audit) {
            audit("attach", client, key, params);
        }

        // do an initial topic load
        Object data = GraphDataExtractor.fetch(theBlastServer, theGraph, key, params);
        return data;

    }

    /**
     * No longer attach to this key i.e. no longer receive any updates
     *
     * @param client
     * @param attachmentId unique attachment Id
     * @throws BlastException
     */
    @Override
    public void detachFrom(BlastServerClient client, String attachmentId) throws BlastException {
        Vertex clientVertex = theGraph.getVertex(client.getClientID());
        if (clientVertex == null) {
            // there is no client
            return;
        }

        Iterable<Edge> edges = clientVertex.getEdges(Direction.OUT, "client-attach");

        for (Edge edge : edges) {
            Vertex child = edge.getVertex(Direction.IN);
            if (child.getProperty("attachmentId").equals(attachmentId)) {
                logger.debug("removed attachment {}", child.getId());
                theGraph.removeVertex(child);
            }
        }
    }

    @Override
    public void detachFromAll(BlastServerClient client) throws BlastException {
        Vertex clientVertex = theGraph.getVertex(client.getClientID());
        if (clientVertex == null) {
            // there is no client
            return;
        }

        Iterable<Edge> edges = clientVertex.getEdges(Direction.OUT, "client-attach");

        for (Edge edge : edges) {
            Vertex child = edge.getVertex(Direction.IN);
            logger.debug("removed attachment {}", child.getId());
            theGraph.removeVertex(child);
        }

    }

    @Override
    public List<Map<String, Object>> getClientAttachments(BlastServerClient client) throws BlastException {
        Vertex clientVertex = theGraph.getVertex(client.getClientID());
        if (clientVertex == null) {
            // there i sno client
            return new ArrayList<>();
        }
        Map<String, Object> clientData = GraphDataExtractor.getData(theGraph, client.getClientID(), false, null);
        List<Map<String, Object>> attachments = (List<Map<String, Object>>) clientData.get("client-attach");
        if (attachments == null) {
            return new ArrayList<>();
        }

        // only want to return a subset of the attachment data
        return BlastUtils.safeStream(attachments).map(attachment -> {
            Map<String, Object> map = new HashMap<>();
            map.put("start-time", attachment.get("start-time"));
            map.put("attachmentId", attachment.get("attachmentId"));

//        boolean haveParams = edge.getProperty("have-params");
//        boolean includeChildren = edge.getProperty("include-children");
//        List<String> includeFields = GraphUtils.getList(edge.getProperty("include-fields"), String.class);
//        List<QueryParameter> queryParams = GraphUtils.getList(edge.getProperty("predicates"), QueryParameter.class);
            map.put("key", attachment.get("key"));

            return map;
        }).collect(Collectors.toList());
    }

    @Override
    public void add(String key, Map<String, Object> data) throws BlastException {
        GraphCollection graphCollection = getCollectionFromPath(key, true);
        Object object = GraphUtils.objectFromMap(data, graphCollection.getDataClass());
        graphCollection.add(object);

    }

    @Override
    public void update(String key, Map<String, Object> data) throws BlastException {
        GraphCollection graphCollection = getCollectionFromPath(key, false);
        Object object = GraphUtils.objectFromMap(data, graphCollection.getDataClass());
        graphCollection.update(object);
    }

    @Override
    public void remove(String key) throws BlastException {
        PathDetails[] pathDetails = PathDetails.splitPath(key);
        if (pathDetails[pathDetails.length - 1].getKeyField() == null) {
            // deleting an array on parent
            throw new BlastException("Can only remove a record by its idendity only e.g. collection/keyField:keyValue");
        } else {
            // deleting a record
            GraphCollection graphCollection = getCollectionFromPath(key, false);
            graphCollection.remove(pathDetails[pathDetails.length - 1].getKeyValue());
        }
    }

    private GraphCollection getCollectionFromPath(String key, boolean requiresCollection) throws BlastException {
        String collectionName = GraphUtils.validatePathAndGetCollectionName(key, requiresCollection);
        return GraphCollection.getCollection(theBlastServer, theGraph, collectionName);
    }

    @Override
    public void setBlastServer(BlastServer blastServer) {
        this.theBlastServer = blastServer;
    }

}

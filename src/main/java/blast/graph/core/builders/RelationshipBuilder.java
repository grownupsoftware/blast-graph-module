package blast.graph.core.builders;

import blast.exception.BlastException;
import blast.server.BlastServer;
import blast.graph.core.GraphCollection;
import blast.graph.core.dataobjects.Relationship;

/**
 * flow pattern for building relationships (edges) without build
 *
 * @author grant
 */
public class RelationshipBuilder {

    private final GraphCollection childCollection;

    private final GraphCollection parentCollection;

    private final BlastServer theBlastServer;

    /**
     * Create a relationship (edge) where child is linked to parent
     *
     * @param blastServer
     * @param childCollection
     * @param parentCollection
     */
    public RelationshipBuilder(BlastServer blastServer, GraphCollection childCollection, GraphCollection parentCollection) {
        this.childCollection = childCollection;
        this.parentCollection = parentCollection;
        this.theBlastServer = blastServer;
    }

    /**
     * the field name on child that is the link to the parent
     *
     * @param fieldname
     * @throws BlastException
     */
    public void using(String fieldname) throws BlastException {
        childCollection.addLink(theBlastServer, new Relationship(parentCollection, fieldname));
    }

}

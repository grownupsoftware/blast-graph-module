package blast.graph.core.builders;

import blast.exception.BlastException;
import blast.graph.core.BlastGraph;
import blast.graph.core.GraphCollection;
import blast.graph.core.utils.GraphUtils;
import blast.utils.BlastUtils;
import java.lang.reflect.Field;
import java.util.List;
import java.util.Optional;

/**
 *
 * @author grant
 * @param <T> Type of data class
 * @param <K> Type of key field
 */
public class CollectionBuilder<T, K> {

    private String name;
    private String keyfield;
    private Class<T> dataclass;
    private String[] includeFields;
    private String[] excludeFields;
    private final BlastGraph blastGraph;

    public CollectionBuilder(BlastGraph blastGraph) {
        this.blastGraph = blastGraph;
    }

    public CollectionBuilder name(String name) {
        this.name = name;
        return this;
    }

    public CollectionBuilder keyfield(String keyfield) {
        this.keyfield = keyfield;
        return this;
    }

    public CollectionBuilder dataclass(Class<T> dataclass) {
        this.dataclass = dataclass;
        return this;
    }

    public CollectionBuilder onlyFields(String... includeFields) {
        this.includeFields = includeFields;
        return this;
    }

    public CollectionBuilder excludeFields(String... excludeFields) {
        this.excludeFields = excludeFields;
        return this;
    }

    public GraphCollection<T, K> build() throws BlastException {
        List<Field> classFields = GraphUtils.getFieldsFor(dataclass);
        Optional<Field> optional = BlastUtils.safeStream(classFields).filter(field -> field.getName().equals(keyfield)).findFirst();
        if (!optional.isPresent()) {
            throw new BlastException("Failed to find type of field [" + keyfield + "]");
        }
        Class<?> keyType = optional.get().getType();
        GraphCollection.validateKeyClass(keyType);
        GraphCollection graphCollection = blastGraph.createCollection(name, keyfield, dataclass, keyType, includeFields, excludeFields);
        return graphCollection;
    }

}

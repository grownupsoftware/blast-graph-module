/*
 * Grownup Software Limited.
 */
package blast.graph.core;

import blast.client.BlastServerClient;
import blast.doc.DocField;
import blast.exception.BlastException;
import blast.graph.core.utils.GraphUtils;
import blast.graph.core.dataobjects.Relationship;
import blast.graph.core.dataobjects.Instruction;
import blast.server.BlastServer;
import blast.graph.core.builders.RelationshipBuilder;
import static blast.graph.core.dataobjects.GraphMessageType.GRAPH_ADD_RESPONSE;
import static blast.graph.core.dataobjects.GraphMessageType.GRAPH_REMOVE_RESPONSE;
import static blast.graph.core.dataobjects.GraphMessageType.GRAPH_UPDATE_RESPONSE;
import blast.graph.core.dataobjects.GraphResponseMessage;
import blast.graph.core.dataobjects.Operation;
import blast.graph.core.dataobjects.QueryParameter;
import blast.graph.core.utils.GraphDataExtractor;
import static blast.graph.core.utils.GraphUtils.prettyPrint;
import blast.log.BlastLogger;
import blast.utils.BlastUtils;
import com.google.common.collect.MapDifference;
import com.google.common.collect.MapDifference.ValueDifference;
import com.google.common.collect.Maps;
import com.tinkerpop.blueprints.Direction;
import com.tinkerpop.blueprints.Edge;
import com.tinkerpop.blueprints.Graph;
import com.tinkerpop.blueprints.Vertex;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

/**
 * A collection of vertices. In essence a vertex that has an edge (relationship)
 * to its children. An example, Users is a collection of User, making it easy to
 * say 'getUsers() and retrieve all users.
 *
 * @author grant
 * @param <T> The class of the data object
 * @param <K> The class of the key
 */
public class GraphCollection<T, K> {

    protected static final BlastLogger logger = BlastLogger.createLogger();

    @DocField(description = "The master vertex of all collections")
    public static final String COLLECTION_MASTER = "root";

    @DocField(description = "Collections connected to root are prefixed with this value and connected to root e.g. 'runners - > 'root")
    public static final String COLLECTION_PREFIX = "collection-";

    @DocField(description = "Edge name for collections to root e.g. 'runners' -> 'root")
    public static final String COLLECTION_EDGE_NAME = "collections";

    @DocField(description = "Edge between a data record its collection e.g. 'runner/id:1' -> 'runners'")
    public static final String COLLECTION_DATA_EDGE = "data";

    @DocField(description = "Edge between a data record and the attachment vertex e.g. 'runner/id:1 -> attachment")
    public static final String COLLECTION_ATTACHMENT_EDGE = "attachments";

    @DocField(description = "Edge between a client vertex and attachmenst e.g. 'client/clientId:abc -> attachment")
    public static final String COLLECTION_CLIENT_ATTACHMENT_EDGE = "client-attach";

    @DocField(description = "Property name for the collection name")
    public static final String COLLECTION_FIELD_NAME = "name";

    @DocField(description = "Property name for the key field that makes the record unique e.g. id")
    public static final String COLLECTION_FIELD_KEYNAME = "keyname";

    @DocField(description = "Property name for the data class name")
    public static final String COLLECTION_FIELD_CLASSNAME = "classname";

    @DocField(description = "Property name for the class of the key i.e. Integer, Long, String")
    public static final String COLLECTION_FIELD_KEYTYPE = "keytype";

    @DocField(description = "Property name to store all the field names")
    public static final String COLLECTION_DATA_FIELD_NAMES = "field-names";

    @DocField(description = "Property name for a list of parent relationships")
    public static final String COLLECTION_FIELD_PARENT_RELATIONSHIPS = "parent-relationships";

    @DocField(description = "Property name for a list of children relationships")
    public static final String COLLECTION_FIELD_CHILD_RELATIONSHIPS = "child-relationships";

    private final String theName;
    private final String theKeyName;
    private final Graph theGraph;
    private final Class<T> theClass;
    private final Class<K> theKeyType;
    private Set<String> theFields;
    private final Vertex theCollectionVertex;
    private final String theEdgeName;

    private Map<String, Object> theChildRelationships = new HashMap<>();
    private Map<String, Object> theParentRelationships = new HashMap<>();

    @DocField(description = "The associated blast server")
    private final BlastServer theBlastServer;

    /**
     * Create a graph collection
     *
     * @param blastServer associated blast server
     * @param graph The Tinkerpop graph
     * @param vertex The collection vertex
     * @param name The name of the collection
     * @param keyName the name of the key field (unique identifier)
     * @param clazz The POJO data classs
     * @param keyType The type of key (String, Long, Integer etc)
     */
    public GraphCollection(BlastServer blastServer, Graph graph, Vertex vertex, String name, String keyName, Class<T> clazz, Class<K> keyType) {
        this.theGraph = graph;
        this.theName = name;
        this.theKeyName = keyName;
        this.theClass = clazz;
        this.theKeyType = keyType;
        this.theCollectionVertex = vertex;
        this.theEdgeName = COLLECTION_DATA_EDGE;
        this.theBlastServer = blastServer;
    }

    /**
     * Create a graph collection
     *
     * @param blastServer
     * @param graph The Tinkerpop graph
     * @param vertex The collection vertex
     * @param name The name of the collection
     * @param keyName the name of the key field (unique identifier)
     * @param clazz The POJO data classs
     * @param keyType The type of key (String, Long, Integer etc)
     * @param parentRelationships Map of relationships (edges) to parents
     * @param childRelationships Map of relationships (edges) to children
     */
    public GraphCollection(BlastServer blastServer, Graph graph, Vertex vertex, String name, String keyName, Class<T> clazz, Class<K> keyType, Map<String, Object> parentRelationships, Map<String, Object> childRelationships) {
        this(blastServer, graph, vertex, name, keyName, clazz, keyType);
        this.theChildRelationships = childRelationships;
        this.theParentRelationships = parentRelationships;
    }

    /**
     * Initialise the vertex property map using a list of fields
     *
     * @param vertex the collection vertex
     * @param includeFields the fields to include
     * @throws BlastException
     */
    public void initialiseWithFields(Vertex vertex, String... includeFields) throws BlastException {
        theFields = BlastUtils.safeStream(includeFields).collect(Collectors.toSet());
        if (!theFields.isEmpty()) {
            theFields.remove("id");
            vertex.setProperty(COLLECTION_DATA_FIELD_NAMES, theFields);
        }
        if (theParentRelationships != null && !theParentRelationships.isEmpty()) {
            vertex.setProperty(COLLECTION_FIELD_PARENT_RELATIONSHIPS, GraphUtils.mapToJson(theParentRelationships));
        }
        if (theChildRelationships != null && !theChildRelationships.isEmpty()) {
            vertex.setProperty(COLLECTION_FIELD_CHILD_RELATIONSHIPS, GraphUtils.mapToJson(theChildRelationships));
        }
    }

    /**
     * Initialise the vertex property map using reflection of the data class
     * (POJO)
     *
     * @param vertex the collection vertex
     * @param excludeFields list of fields to exclude
     * @throws BlastException
     */
    public void initialiseFromClass(Vertex vertex, String... excludeFields) throws BlastException {
        try {
            T dataObject = theClass.newInstance();
            theFields = GraphUtils.getFieldNames(dataObject);
            BlastUtils.safeStream(excludeFields).forEach(field -> {
                theFields.remove(field);
            });
            if (!theFields.isEmpty()) {
                vertex.setProperty(COLLECTION_DATA_FIELD_NAMES, theFields);
            }
            if (theParentRelationships != null && !theParentRelationships.isEmpty()) {
                vertex.setProperty(COLLECTION_FIELD_PARENT_RELATIONSHIPS, GraphUtils.mapToJson(theParentRelationships));
            }
            if (theChildRelationships != null && !theChildRelationships.isEmpty()) {
                vertex.setProperty(COLLECTION_FIELD_CHILD_RELATIONSHIPS, GraphUtils.mapToJson(theChildRelationships));
            }

        } catch (InstantiationException | IllegalAccessException ex) {
            throw new BlastException("Failed to create instance of class [" + theClass.getCanonicalName() + "] - does it have a default constructor?");
        }
    }

    public void addLink(BlastServer blastServer, Relationship relationship) throws BlastException {
        Vertex parent = theGraph.getVertex(COLLECTION_PREFIX + relationship.getParentCollection().getName());
        if (parent == null) {
            throw new BlastException("Cannot find parent collection for: " + COLLECTION_PREFIX + relationship.getParentCollection().getName());
        }
        if (theParentRelationships == null) {
            theParentRelationships = new HashMap<>();
        }
        theParentRelationships.put(this.getName(), relationship.getFieldname());
        theCollectionVertex.setProperty(COLLECTION_FIELD_PARENT_RELATIONSHIPS, GraphUtils.mapToJson(theParentRelationships));

        // update the parent
        GraphCollection parentCollection = getCollection(blastServer, relationship.getParentCollection().getName());
        parentCollection.addChildRelationship(this.getName(), relationship.getFieldname());

    }

    /**
     * On the parent vertex add the relationship to its collection of
     * relationships
     *
     * @param collectionName the child collection name
     * @param childFieldName the field that is the link between the two
     * @throws BlastException
     */
    private void addChildRelationship(String collectionName, String childFieldName) throws BlastException {
        if (theChildRelationships == null) {
            theChildRelationships = new HashMap<>();
        }
        theChildRelationships.put(collectionName, childFieldName);
        theCollectionVertex.setProperty(COLLECTION_FIELD_CHILD_RELATIONSHIPS, GraphUtils.mapToJson(theChildRelationships));
    }

    /**
     * Get a GraphCollection
     *
     * @param blastServer Associated blast server
     * @param collectionName The name of the collection to get
     * @return
     * @throws BlastException
     */
    public GraphCollection getCollection(BlastServer blastServer, String collectionName) throws BlastException {
        return getCollection(blastServer, theGraph, collectionName);
    }

    /**
     * statically get a Graph Collection
     *
     * @param blastServer associated blast server
     * @param graph The Tinkerpop graph that has this collection
     * @param collectionName the collection name
     * @return
     * @throws BlastException
     */
    public static GraphCollection getCollection(BlastServer blastServer, Graph graph, String collectionName) throws BlastException {

        Vertex vertex = graph.getVertex(COLLECTION_PREFIX + collectionName);

        if (vertex == null) {
            throw new BlastException("Failed to find collection: " + collectionName);
        }

        String classname = vertex.getProperty(COLLECTION_FIELD_CLASSNAME);
        if (classname == null) {
            throw new BlastException("Associated data class is null");
        }
        Class<?> clazz;
        try {
            clazz = Class.forName(classname);
        } catch (ClassNotFoundException ex) {
            throw new BlastException("Failed to find class: " + classname, ex);
        }

        String keyType = vertex.getProperty(COLLECTION_FIELD_KEYTYPE);
        if (keyType == null) {
            throw new BlastException("Associated data class is null");
        }
        Class<?> keyClass;
        try {
            keyClass = Class.forName(keyType);
        } catch (ClassNotFoundException ex) {
            throw new BlastException("Failed to find class: " + classname, ex);
        }

        String name = vertex.getProperty(COLLECTION_FIELD_NAME);
        String keyname = vertex.getProperty(COLLECTION_FIELD_KEYNAME);
        Map<String, Object> parentRelationShips = GraphUtils.jsonToMap(vertex.getProperty(COLLECTION_FIELD_PARENT_RELATIONSHIPS));
        Map<String, Object> childRelationShips = GraphUtils.jsonToMap(vertex.getProperty(COLLECTION_FIELD_CHILD_RELATIONSHIPS));

        GraphCollection graphCollection = new GraphCollection(blastServer, graph, vertex, name, keyname, clazz, keyClass, parentRelationShips, childRelationShips);
        Set<String> fieldnames = vertex.getProperty(COLLECTION_DATA_FIELD_NAMES);
        graphCollection.initialiseWithFields(vertex, fieldnames.toArray(new String[fieldnames.size()]));

        return graphCollection;

    }

    /**
     * Build a relationship - starts with child collection
     *
     * @param parentCollection
     * @return
     * @throws blast.exception.BlastException
     */
    public RelationshipBuilder linkedTo(GraphCollection parentCollection) throws BlastException {
        if (theBlastServer == null) {
            throw new BlastException("No Blast Server - please set the server on the graph");
        }

        RelationshipBuilder relationshipBuilder = new RelationshipBuilder(theBlastServer, this, parentCollection);
        return relationshipBuilder;
    }

    /**
     * Add an entity to the collection
     *
     * @param dataObject the data POJO
     * @throws BlastException
     */
    public void add(T dataObject) throws BlastException {
        if (theBlastServer == null) {
            throw new BlastException("No Blast Server - please set the server on the graph");
        }
        Map<String, Object> map = GraphUtils.objectToMap(dataObject);
        K keyValue = (K) map.get(theKeyName);

        if (keyValue == null) {
            throw new BlastException("No key field found");
        }

        Vertex currentRecord;
        try {
            currentRecord = theGraph.addVertex(keyValue);
        } catch (IllegalArgumentException ex) {
            throw new BlastException("Failed to add", ex);
        }
        BlastUtils.safeStream(theFields).forEach(fieldname -> {
            Object value = map.get(fieldname);
            if (value != null) {
                try {
                    currentRecord.setProperty(fieldname, value);
                } catch (IllegalArgumentException ex) {
                    logger.warn("Failed to set field name: {} [Note: some implementations do not like empty collections] - error {} ", fieldname, ex.getMessage());
                }
            }
        });

        // create the link to parent e.g. entity->user
        theGraph.addEdge(null, theCollectionVertex, currentRecord, theEdgeName);

        // check if any parent relationships and add
        BlastUtils.safeStream(theParentRelationships.entrySet()).forEach(entry -> {

            try {
                String relationshipName = entry.getKey();

                GraphCollection parentCollection = getCollection(theBlastServer, relationshipName);

                // key field on child that links to parent
                Object value = GraphUtils.getKeyValueFromObject(parentCollection.getKeyType(), map.get((String) entry.getValue()));

                Vertex parentVertex = theGraph.getVertex(value);

                logger.debug("=== adding child-id: {} => parent-id: {}[{}] on relation-ship-name: {}", keyValue, entry.getValue(), value, relationshipName);
                theGraph.addEdge(null, parentVertex, currentRecord, relationshipName);

            } catch (BlastException ex) {
                Logger.getLogger(GraphCollection.class.getName()).log(Level.SEVERE, null, ex);
            }

        });

        Map<String, Object> newRecord = GraphDataExtractor.getData(theGraph, currentRecord, false, null);
        logger.debug("adding record: {}", newRecord);

        StringBuilder keyBuilder = new StringBuilder();
        List<Instruction> listInstructions = new ArrayList<>();

        updateRootAttachments(currentRecord, Operation.ADD, null, newRecord, listInstructions);

        bubbleUp(null, currentRecord, Operation.ADD, listInstructions, null, keyBuilder, newRecord);
        logger.debug("add instructions: {}", prettyPrint(listInstructions));
    }

    /**
     * Update an entity in the collection - if not in the collection it will add
     *
     * @param dataObject
     * @throws blast.core.exception.BlastException
     */
    public void update(T dataObject) throws BlastException {
        Map<String, Object> newValues = getValidFieldMap(GraphUtils.objectToMap(dataObject));
        K keyValue = (K) newValues.get(theKeyName);

        Vertex currentRecord = theGraph.getVertex(keyValue);

        Map<String, Object> oldValues = getValidFieldMap(convertToMap(currentRecord));

        MapDifference<String, Object> mapDifference = Maps.difference(
                newValues, oldValues);

        Map<String, ValueDifference<Object>> sameKeyDifferentValue = mapDifference
                .entriesDiffering();

        List<Map<String, Object>> listOfChanges = new ArrayList<>();
        BlastUtils.safeStream(sameKeyDifferentValue.entrySet()).forEach(entry -> {
            // 1st update the record
            currentRecord.setProperty(entry.getKey(), entry.getValue().leftValue());

            Map<String, Object> change = new HashMap<>();
            change.put("name", entry.getKey());
            change.put("value", entry.getValue().leftValue());
            change.put("old", entry.getValue().rightValue());
            listOfChanges.add(change);
        });

        if (listOfChanges.isEmpty()) {
            logger.debug("There are no changes - not updating");
        } else {
            logger.debug("listOfChanges: {}", listOfChanges);

            List<Instruction> listInstructions = new ArrayList<>();

            updateRootAttachments(currentRecord, Operation.UPDATE, listOfChanges, null, listInstructions);

            StringBuilder keyBuilder = new StringBuilder();
            bubbleUp(null, currentRecord, Operation.UPDATE, listInstructions, listOfChanges, keyBuilder, null);

            logger.debug("Update instructions: {}", prettyPrint(listInstructions));
        }
    }

    public void remove(String keyValue) throws BlastException {
        logger.debug("Removing: {}", keyValue);
        Object key = GraphUtils.getKeyValue(theKeyType, keyValue);

        Vertex currentRecord = theGraph.getVertex(key);

        // need to inform all that this will be removed
        // delete the vertex - this should remove all edges etc
        logger.debug("Removing: {}", currentRecord);

        StringBuilder keyBuilder = new StringBuilder();
        List<Instruction> listInstructions = new ArrayList<>();

        // bubble up, informing all
        bubbleUp(null, currentRecord, Operation.REMOVE, listInstructions, null, keyBuilder, null);
        logger.debug("remove instructions: {}", listInstructions);

        // we have to manually remove client-attachments i.e. we have an attachment vertex not connected to any record
        Iterable<Edge> edges = currentRecord.getEdges(Direction.OUT, COLLECTION_ATTACHMENT_EDGE);
        for (Edge edge : edges) {
            Vertex attachVertex = edge.getVertex(Direction.IN);
            theGraph.removeVertex(attachVertex);
        }

        theGraph.removeVertex(currentRecord);
    }

    private void updateRootAttachments(Vertex currentRecord, Operation operation, List<Map<String, Object>> listOfChanges, Map<String, Object> newRecord, List<Instruction> listInstructions) throws BlastException {
        // need to update attachmenst on root        
        Vertex master = theGraph.getVertex(COLLECTION_MASTER);

        Iterable<Edge> edges = currentRecord.getEdges(Direction.IN, COLLECTION_DATA_EDGE);
        String pathCollectionName = "";
        for (Edge edge : edges) {
            Vertex v = edge.getVertex(Direction.OUT);
            pathCollectionName = ((String) v.getId()).replace("collection-", "");
        }

        // FIXME s/be key field not id
        String path = pathCollectionName + "/" + "id" + ":" + currentRecord.getId().toString();

        edges = master.getEdges(Direction.OUT, COLLECTION_ATTACHMENT_EDGE);
        for (Edge edge : edges) {
            Vertex attachVertex = edge.getVertex(Direction.IN);
            logger.debug("bubble up edge: {} parent: {}", edge.getLabel(), attachVertex.getId());
            Instruction instruction = new Instruction(operation);

            instruction.setPath(path);
            instruction.setCollection("root");

            switch (operation) {
                case ADD:
                    instruction.setRecord(newRecord);
                    break;
                case UPDATE:
                    instruction.setChanges(listOfChanges);
                    break;
                case REMOVE:
                    break;
                default:
                    throw new AssertionError(operation.name());

            }
            listInstructions.add(instruction);
            sendData(attachVertex, instruction);
        }
    }

    private void sendData(Vertex attachVertex, Instruction instruction) throws BlastException {

        String attachmentId = attachVertex.getProperty("attachmentId");
        String key = attachVertex.getProperty("key");

        boolean haveParams = attachVertex.getProperty("have-params");
        boolean includeChildren = true;
        List<String> includeFields = null;
        List<QueryParameter> queryParameters = null;

        if (haveParams) {
            includeChildren = attachVertex.getProperty("include-children");

            String flds = attachVertex.getProperty("include-fields");
            if (flds != null) {
                includeFields = GraphUtils.jsonToList(flds, String.class);
            }

            String preds = attachVertex.getProperty("predicates");
            if (preds != null) {
                queryParameters = GraphUtils.jsonToList(preds, QueryParameter.class);
            }
        }

        //logger.info("--------------------- haveParams: {} include-Children:{}", haveParams, includeChildren);
        GraphResponseMessage graphMessage;
        switch (instruction.getOperation()) {
            case ADD:
                graphMessage = new GraphResponseMessage(GRAPH_ADD_RESPONSE, attachmentId, key, instruction);
                break;
            case UPDATE:
                graphMessage = new GraphResponseMessage(GRAPH_UPDATE_RESPONSE, attachmentId, key, instruction);
                break;
            case REMOVE:
                graphMessage = new GraphResponseMessage(GRAPH_REMOVE_RESPONSE, attachmentId, key, instruction);
                break;
            default:
                throw new AssertionError(instruction.getOperation().name());

        }
        logger.debug("send graphMessage: {} ", graphMessage);

        Iterable<Edge> edges = attachVertex.getEdges(Direction.IN, COLLECTION_CLIENT_ATTACHMENT_EDGE);
        for (Edge edge : edges) {
            Vertex blastClient = edge.getVertex(Direction.OUT);
            sendMessageToClient((String) blastClient.getId(), graphMessage);
        }
    }

    private void sendMessageToClient(String clientId, GraphResponseMessage graphMessage) throws BlastException {
        if (theBlastServer == null) {
            throw new BlastException("No Blast Server - please set the server on the graph");
        }
        BlastServerClient blastServerClient = theBlastServer.getClientByID(clientId);
        if (blastServerClient == null) {
            logger.warn("Client not found for id: {}", clientId);
            // mmm lets see who is connected
            BlastUtils.safeStream(theBlastServer.getClients()).forEach(client -> {
                logger.info("Client connected: {}", client.getClientID());
            });
            return;
        }
        logger.debug("sending message to: {} message - command: {} key: {}", clientId, graphMessage.getCmd(), graphMessage.getKey());
        blastServerClient.queueMessage(graphMessage);
    }

    private void bubbleUp(String path,
            Vertex vertex,
            Operation operation,
            List<Instruction> listInstructions,
            List<Map<String, Object>> listOfChanges,
            StringBuilder keyBuilder,
            Map<String, Object> newRecord) throws BlastException {

        logger.debug("bubbleup - touched: {}", vertex.getId());
        Instruction instruction = new Instruction(operation);
        if (path == null) {
            path = "";
        }
        Iterable<Edge> edges;
        edges = vertex.getEdges(Direction.IN, COLLECTION_DATA_EDGE);
        String pathCollectionName = "";
        for (Edge edge : edges) {
            Vertex v = edge.getVertex(Direction.OUT);
            pathCollectionName = ((String) v.getId()).replace("collection-", "");
        }

        // FIXME s/be key field not id
        path = pathCollectionName + "/" + "id" + ":" + vertex.getId().toString() + (path.isEmpty() ? "" : ("/" + path));
        keyBuilder.insert(0, path);

        instruction.setPath(path);
        switch (operation) {
            case ADD:
                instruction.setRecord(newRecord);
                break;
            case UPDATE:
                instruction.setChanges(listOfChanges);
                break;
            case REMOVE:
                break;
            default:
                throw new AssertionError(operation.name());

        }
        listInstructions.add(instruction);

        edges = vertex.getEdges(Direction.OUT, COLLECTION_ATTACHMENT_EDGE);
        for (Edge edge : edges) {
            logger.info("------------- edge: {}", edge.getLabel());
            Vertex attachVertex = edge.getVertex(Direction.IN);
            sendData(attachVertex, instruction);
        }

        edges = vertex.getEdges(Direction.IN);
        for (Edge edge : edges) {
            switch (edge.getLabel()) {
                case COLLECTION_EDGE_NAME:
                case COLLECTION_DATA_EDGE:
                    Vertex collectonVertex = edge.getVertex(Direction.OUT);
                    Instruction topLevelCollectionInstruction = new Instruction(operation);
                    topLevelCollectionInstruction.setCollection(((String) collectonVertex.getId()).replace("collection-", ""));
                    topLevelCollectionInstruction.setChanges(listOfChanges);
                    topLevelCollectionInstruction.setPath(path);
                    topLevelCollectionInstruction.setRecord(newRecord);
                    listInstructions.add(topLevelCollectionInstruction);

                    updateCollectionAttachments(collectonVertex, topLevelCollectionInstruction);

                    break;
                default:
                    Vertex parent = edge.getVertex(Direction.OUT);
                    logger.debug("bubble up edge: {} parent: {}", edge.getLabel(), parent.getId());
                    bubbleUp(path, parent, operation, listInstructions, listOfChanges, keyBuilder, newRecord);
            }
        }
    }

    private void updateCollectionAttachments(Vertex collectonVertex, Instruction instruction) throws BlastException {
        logger.debug("Informing attachments on collection: {}", collectonVertex.getId());
        Iterable<Edge> edges;
        edges = collectonVertex.getEdges(Direction.OUT, COLLECTION_ATTACHMENT_EDGE);
        for (Edge edge : edges) {
            Vertex attachVertex = edge.getVertex(Direction.IN);
            logger.debug("update collection attachments parent: {} edge: {} attachId: {}", collectonVertex.getId(), edge.getLabel(), attachVertex.getId());
            sendData(attachVertex, instruction);
        }
    }

    public static <K, V> Map<K, V> mapDifference(Map<? extends K, ? extends V> left, Map<? extends K, ? extends V> right) {
        Map<K, V> difference = new HashMap<>();
        difference.putAll(left);
        difference.putAll(right);
        difference.entrySet().removeAll(right.entrySet());
        return difference;
    }

    private Map<String, Object> convertToMap(Vertex vertex) {
        Map<String, Object> map = BlastUtils.safeStream(vertex.getPropertyKeys()).collect(
                Collectors.toMap(propertyKey -> propertyKey, propertyKey -> vertex.getProperty(propertyKey)));
        map.put(theKeyName, vertex.getId());
        return map;
    }

    public static void validateKeyClass(Class<?> keyType) throws BlastException {
        switch (keyType.getCanonicalName()) {
            case "java.lang.Long":
            case "java.lang.Integer":
            case "java.lang.String":
                // valid key types
                //TODO - check more keytypes should be allowed
                return;
        }
        throw new BlastException("Key type of [" + keyType.getCanonicalName() + "] is not supported");
    }

    public String getName() {
        return theName;
    }

    public String getKeyName() {
        return theKeyName;
    }

    public Class<K> getKeyType() {
        return theKeyType;
    }

    public Class<T> getDataClass() {
        return theClass;
    }

    @Override
    public String toString() {
        return "GraphCollection{" + "theName=" + theName + ", theKeyName=" + theKeyName + ", theClass=" + theClass.getCanonicalName() + ", theKeyType=" + theKeyType.getCanonicalName() + ", theFields=" + theFields + ", theEdgeName=" + theEdgeName + ", theChildRelationships=" + theChildRelationships + ", theParentRelationships=" + theParentRelationships + '}';
    }

    public Set<String> getFieldNames() {
        return theFields;
    }

    public Map<String, Object> getValidFieldMap(Map<String, Object> currentMap) {
        Map<String, Object> newMap = new HashMap<>();
        BlastUtils.safeStream(theFields).forEach(fieldname -> {
            newMap.put(fieldname, currentMap.get(fieldname));
        });
        // add the key 
        newMap.put(theKeyName, currentMap.get(theKeyName));
        return newMap;
    }

}

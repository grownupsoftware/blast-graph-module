package blast.graph.core.dataobjects;

import blast.doc.DocField;
import java.util.List;

/**
 *
 * @author grant
 */
public class PathParameters {

    @DocField(description = "include child collections")
    private boolean includeChildren;

    @DocField(description = "only return this list of fields")
    private List<String> fields;

    @DocField(description = "Queries to be applied to the result")
    private List<QueryParameter> predicates;

    public PathParameters() {
    }

    public boolean hasIncludeChildren() {
        return includeChildren;
    }

    public void setIncludeChildren(boolean includeChildren) {
        this.includeChildren = includeChildren;
    }

    public List<String> getFields() {
        return fields;
    }

    public void setFields(List<String> fields) {
        this.fields = fields;
    }

    public List<QueryParameter> getPredicates() {
        return predicates;
    }

    public void setPredicates(List<QueryParameter> predicates) {
        this.predicates = predicates;
    }

    @Override
    public String toString() {
        return "PathParameters{" + "includeChildren=" + includeChildren + ", fields=" + fields + ", predicates=" + predicates + '}';
    }

}

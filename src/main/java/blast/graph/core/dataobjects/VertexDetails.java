package blast.graph.core.dataobjects;

import com.tinkerpop.blueprints.Vertex;

/**
 *
 * @author grant
 */
public class VertexDetails {

    private final boolean isRoot;
    private final boolean isCollection;
    private final Vertex vertex;
    private final String fullPath;
    private final String[] queryParams;

    public VertexDetails(boolean isCollection, Vertex vertex, String fullPath, String[] queryParams) {
        this.isCollection = isCollection;
        this.vertex = vertex;
        this.fullPath = fullPath;
        this.queryParams = queryParams;
        this.isRoot = false;
    }

    public VertexDetails(Vertex vertex) {
        this.isRoot = true;
        this.isCollection = false;
        this.vertex = vertex;
        this.fullPath = "";
        this.queryParams = null;
    }

    public boolean isCollection() {
        return isCollection;
    }

    public Vertex getVertex() {
        return vertex;
    }

    public String getFullPath() {
        return fullPath;
    }

    public String[] getQueryParams() {
        return queryParams;
    }

    public boolean isRoot() {
        return isRoot;
    }
}

/*
 * Grownup Software Limited.
 */
package blast.graph.core.dataobjects;

import blast.doc.DocField;
import java.util.List;
import java.util.Map;

/**
 *
 * @author grant
 */
public class Instruction {

    @DocField(description = "The operation")
    private Operation operation;
    
    @DocField(description = "The path of the change")
    private String path;

    @DocField(description = "top level collection name")
    private String collection;

    @DocField(description = "list of field changes")
    private List<Map<String, Object>> changes;

    @DocField(description = "New Record")
    private Map<String, Object> record;

    public Instruction() {
    }

    public Instruction(Operation operation) {
        this.operation = operation;
    }

    public Operation getOperation() {
        return operation;
    }

    public void setOperation(Operation operation) {
        this.operation = operation;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public List<Map<String, Object>> getChanges() {
        return changes;
    }

    public void setChanges(List<Map<String, Object>> changes) {
        this.changes = changes;
    }

    public String getCollection() {
        return collection;
    }

    public void setCollection(String collection) {
        this.collection = collection;
    }

    public Map<String, Object> getRecord() {
        return record;
    }

    public void setRecord(Map<String, Object> record) {
        this.record = record;
    }

    @Override
    public String toString() {
        return "Instruction{" + "operation=" + operation + ", path=" + path + ", collection=" + collection + ", changes=" + changes + ", record=" + record + '}';
    }

}

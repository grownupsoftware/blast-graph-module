package blast.graph.core.dataobjects;

import blast.doc.DocField;

/**
 *
 * @author grant
 */
public class QueryParameter {

    @DocField(description = "The Field to do the comparison on")
    private String field;

    @DocField(description = "The operand e.g. = != > < ")
    private String operand;

    @DocField(description = "The value to check")
    private String value;

    public QueryParameter() {
    }

    public QueryParameter(String field, String operand, String value) {
        this.field = field;
        this.operand = operand;
        this.value = value;
    }

    public String getField() {
        return field;
    }

    public void setField(String field) {
        this.field = field;
    }

    public String getOperand() {
        return operand;
    }

    public void setOperand(String operand) {
        this.operand = operand;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "QueryParameter{" + "field=" + field + ", operand=" + operand + ", value=" + value + '}';
    }

}

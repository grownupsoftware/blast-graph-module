package blast.graph.core.dataobjects;

/**
 *
 * @author grant
 */
public enum Operation {
    ADD,
    UPDATE,
    REMOVE
}

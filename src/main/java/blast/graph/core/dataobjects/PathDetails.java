package blast.graph.core.dataobjects;

import blast.exception.BlastException;
import static blast.graph.core.GraphCollection.COLLECTION_MASTER;
import blast.log.BlastLogger;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author grant
 */
public class PathDetails {

    protected final BlastLogger logger = BlastLogger.createLogger();

    private String collection;
    private String keyField;
    private String keyValue;
    private String[] queryParams;
    private boolean isRoot;
    private boolean isCollectionOnly;

    public PathDetails() {

    }

    public PathDetails(boolean isRoot) {
        this.isRoot = true;
        this.collection = COLLECTION_MASTER;
        this.isCollectionOnly = false;
    }

    public PathDetails(String collection) {
        this.isRoot = false;
        this.collection = collection;
        this.isCollectionOnly = true;
    }

    public String getCollection() {
        return collection;
    }

    public void setCollection(String collection) {
        this.collection = collection;
    }

    public String getKeyField() {
        return keyField;
    }

    public void setKeyField(String keyField) {
        this.keyField = keyField;
    }

    public String getKeyValue() {
        return keyValue;
    }

    public void setKeyValue(String keyValue) {
        this.keyValue = keyValue;
    }

    public String[] getQueryParams() {
        return queryParams;
    }

    public void setQueryParams(String[] queryParams) {
        this.queryParams = queryParams;
    }

    public boolean isRoot() {
        return isRoot;
    }

    public void setRoot(boolean isRoot) {
        this.isRoot = isRoot;
    }

    public boolean isCollectionOnly() {
        return isCollectionOnly;
    }

    public void setCollectionOnly(boolean isCollectionOnly) {
        this.isCollectionOnly = isCollectionOnly;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("PathDetails").append("{");
        if (isRoot) {
            builder.append("root=true").append("}");
            return builder.toString();
        }
        if (isCollectionOnly) {
            builder.append("collection=").append(collection).append("}");
            return builder.toString();
        }
        builder.append("collection=").append(collection);
        if (keyField != null) {
            builder.append(",key=").append(keyField);

        }
        if (keyValue != null) {
            builder.append(",value=").append(keyValue);

        }
        if (queryParams != null) {
            builder.append(",query=").append(Arrays.asList(queryParams));
        }
        builder.append("}");
        return builder.toString();
    }

    public static PathDetails[] splitPath(String key) throws BlastException {
        List<PathDetails> pathBreakdown = new LinkedList<>();

        if (key == null || key.trim().isEmpty() || key.equals("root")) {
            // root
            pathBreakdown.add(new PathDetails(true));
            return pathBreakdown.toArray(new PathDetails[pathBreakdown.size()]);
        }

        if (key.startsWith("/")) {
            throw new BlastException("key cannot start with / - must specify collection e.g. collection/keyname:value");
        }

        String[] splits = key.split("/");

        if (splits[0].contains(":")) {
            throw new BlastException("must specify collection e.g. collection/keyname:value");
        }

        // check if it is collection only
        if (splits.length == 1) {
            // collection
            pathBreakdown.add(new PathDetails(key));
            return pathBreakdown.toArray(new PathDetails[pathBreakdown.size()]);
        }

        PathDetails pathDetails = new PathDetails();
        pathDetails.setCollection(splits[0]);
        pathBreakdown.add(pathDetails);

        for (int x = 1; x < splits.length; x++) {
            if (splits[x].contains(":")) {
                String[] keySplit = splits[x].split(":");
                if (keySplit.length != 2) {
                    throw new BlastException("look up must be in form keyname:value - found: " + splits[x]);
                }
                pathDetails.setKeyField(keySplit[0]);
                pathDetails.setKeyValue(keySplit[1]);
            } else {
                if (splits[x].startsWith("?")) {
                    pathDetails.setQueryParams(splits[x].substring(1).split(","));
                } else {
                    pathDetails = new PathDetails();
                    pathDetails.setCollection(splits[x]);
                    pathBreakdown.add(pathDetails);
                }
            }
        }

        return pathBreakdown.toArray(new PathDetails[pathBreakdown.size()]);

    }
}

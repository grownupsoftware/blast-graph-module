package blast.graph.core.dataobjects;

import blast.message.BlastMessage;

/**
 *
 * @author grant
 */
public class GraphResponseMessage implements BlastMessage {

    protected Long correlationId;

    private String key;

    private String attachmentId;

    private Instruction instruction;

    private Object data;
    private String status;
    private GraphMessageType graphMessageType;

    public GraphResponseMessage() {
    }

    public static GraphResponseMessage OkMessage(Long correlationId) {
        GraphResponseMessage message = new GraphResponseMessage();
        message.setGraphMessageType(GraphMessageType.GRAPH_OK_RESPONSE);
        message.setCorrelationId(correlationId);
        message.setStatus("success");
        return message;
    }

    public static GraphResponseMessage FailMessage(Long correlationId, String errorMessage) {
        GraphResponseMessage message = new GraphResponseMessage();
        message.setGraphMessageType(GraphMessageType.GRAPH_FAIL_RESPONSE);
        message.setCorrelationId(correlationId);
        message.setStatus("failure");
        message.setData(errorMessage);
        return message;
    }

    public GraphResponseMessage(GraphMessageType graphMessageType, String attachmentId, String key, Instruction instruction) {
        this.graphMessageType = graphMessageType;
        this.correlationId = null;
        this.attachmentId = attachmentId;
        this.key = key;
        this.instruction = instruction;
    }

    public GraphResponseMessage(GraphMessageType graphMessageType, Long correlationId, String attachmentId, String key, Object data) {
        this.graphMessageType = graphMessageType;
        this.correlationId = correlationId;
        this.attachmentId = attachmentId;
        this.key = key;
        this.data = data;
    }

    public GraphResponseMessage(GraphMessageType graphMessageType, Long correlationId, String key) {
        this.graphMessageType = graphMessageType;
        this.correlationId = correlationId;
        this.key = key;
    }

    public GraphResponseMessage(GraphMessageType graphMessageType, Long correlationId) {
        this.graphMessageType = graphMessageType;
        this.correlationId = correlationId;
    }

    public GraphResponseMessage(GraphMessageType graphMessageType, Long correlationId, String key, Object data) {
        this.graphMessageType = graphMessageType;
        this.correlationId = correlationId;
        this.key = key;
        this.data = data;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public Instruction getInstruction() {
        return instruction;
    }

    public void setInstruction(Instruction instruction) {
        this.instruction = instruction;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Long getCorrelationId() {
        return correlationId;
    }

    public void setCorrelationId(Long correlationId) {
        this.correlationId = correlationId;
    }

    public GraphMessageType getGraphMessageType() {
        return graphMessageType;
    }

    public void setGraphMessageType(GraphMessageType graphMessageType) {
        this.graphMessageType = graphMessageType;
    }

    public String getAttachmentId() {
        return attachmentId;
    }

    public void setAttachmentId(String attachmentId) {
        this.attachmentId = attachmentId;
    }

    @Override
    public String toString() {
        return "GraphResponseMessage{" + "correlationId=" + correlationId + ", key=" + key + ", attachmentId=" + attachmentId + ", instruction=" + instruction + ", data=" + data + ", status=" + status + ", graphMessageType=" + graphMessageType + '}';
    }

    @Override
    public String getCmd() {
        return graphMessageType.getValue();
    }

}

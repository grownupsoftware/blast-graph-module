/*
 * Grownup Software Limited.
 */
package blast.graph.core.dataobjects;

import blast.graph.core.GraphCollection;

/**
 *
 * @author grant
 */
public class Relationship {

    private final String fieldname;
    private final GraphCollection theParentCollection;

    public Relationship(GraphCollection parentCollection, String fieldname) {
        this.fieldname = fieldname;
        this.theParentCollection = parentCollection;
    }

    public String getFieldname() {
        return fieldname;
    }

    public GraphCollection getParentCollection() {
        return theParentCollection;
    }

    @Override
    public String toString() {
        return "Relationship{" + "fieldname=" + fieldname + ", theChildCollection=" + theParentCollection + '}';
    }

}

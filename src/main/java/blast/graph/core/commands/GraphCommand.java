package blast.graph.core.commands;

import blast.command.CommandMessage;
import blast.doc.DocField;
import blast.graph.core.dataobjects.PathParameters;
import java.util.Map;

/**
 *
 * @author grant
 */
public class GraphCommand extends CommandMessage {

    @DocField(description = "Correlation Id")
    private Long correlationId;

    @DocField(description = "key to graph")
    private String key;

    @DocField(description = "additional params")
    private PathParameters parameters;

    @DocField(description = "additional data")
    private Map<String, Object> data;

    @DocField(description = "attachment id")
    private String attachmentId;

    public Long getCorrelationId() {
        return correlationId;
    }

    public void setCorrelationId(Long correlationId) {
        this.correlationId = correlationId;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public PathParameters getParameters() {
        return parameters;
    }

    public void setParameters(PathParameters parameters) {
        this.parameters = parameters;
    }

    public Map<String, Object> getData() {
        return data;
    }

    public void setData(Map<String, Object> data) {
        this.data = data;
    }

    public String getAttachmentId() {
        return attachmentId;
    }

    public void setAttachmentId(String attachmentId) {
        this.attachmentId = attachmentId;
    }

    @Override
    public String toString() {
        return "GraphCommand{" + "correlationId=" + correlationId + ", key=" + key + ", parameters=" + parameters + ", data=" + data + ", attachmentId=" + attachmentId + '}';
    }

}

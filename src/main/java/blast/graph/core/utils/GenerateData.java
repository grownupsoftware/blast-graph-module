package blast.graph.core.utils;

import blast.exception.BlastException;
import blast.graph.client.GraphExternalClient;
import blast.graph.example.model.EventDO;
import blast.graph.example.model.MarketDO;
import blast.graph.example.model.RunnerDO;
import blast.log.BlastLogger;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Random;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

/**
 *
 * @author grant
 */
public class GenerateData {

    protected static final BlastLogger logger = BlastLogger.createLogger();

    private static GraphExternalClient theExternalClient = null;

    Random random = new Random();

    public static void main(String... args) {
        try {
            GenerateData generateData = new GenerateData();

            generateData.setup();
            generateData.updateEvent();
            generateData.addEvent();
            generateData.addRunner();

            theExternalClient.close();

        } catch (Exception ex) {
            logger.error("error", ex);
//        } finally {
//            BlastUtils.sleep(1000);
        }
    }

    public void setup() throws BlastException {

        try {
            // we are not going to connect - so no server needed
            theExternalClient = new GraphExternalClient(new URI("ws://127.0.0.1:8081/blast"));
            theExternalClient.connect().get();
        } catch (InterruptedException | ExecutionException | URISyntaxException ex) {
            logger.error("URI format exception", ex);
            throw new BlastException("URI format exception", ex);
        }
    }

    public void updateEvent() {
        try {
            Long eventId = getUniqueId();
            theExternalClient.update("events/id:100", createEvent(100l, "event " + eventId, "open")).get(1, TimeUnit.SECONDS);
        } catch (Exception ex) {
            logger.error("error", ex);
        }
    }

    public void addEvent() {
        try {
            Long eventId = getUniqueId();
            theExternalClient.add("events", createEvent(eventId, "event " + eventId, "open")).get(1, TimeUnit.SECONDS);
        } catch (Exception ex) {
            logger.error("error", ex);
        }
    }

    public void addRunner() {
        try {
            Long runnerId = getUniqueId();
            theExternalClient.add("runners", createRunner(100l, 101l, runnerId, "runner " + runnerId, "open")).get(1, TimeUnit.SECONDS);
            //theExternalClient.add("events", createEvent(getUniqueId(), "event " + getUniqueId(), "open")).get(1, TimeUnit.SECONDS);
        } catch (Exception ex) {
            logger.error("error", ex);
        }
    }

    private void createData(int numberEvents, int numMarketsPerEvent, int numRunnersPerMarket) throws Exception {

        for (int eventCnt = 0; eventCnt < numberEvents; eventCnt++) {
            Long eventId = getUniqueId();
            theExternalClient.add("events", createEvent(eventId, "event " + eventId, "open")).get(1, TimeUnit.SECONDS);
            for (int marketCnt = 0; marketCnt < numMarketsPerEvent; marketCnt++) {
                Long marketId = getUniqueId();
                theExternalClient.add("markets", createMarket(eventId, marketId, "market " + marketId, "open")).get(1, TimeUnit.SECONDS);
                for (int runnerCnt = 0; runnerCnt < numRunnersPerMarket; runnerCnt++) {
                    Long runnerId = getUniqueId();
                    theExternalClient.add("runners", createRunner(eventId, marketId, runnerId, "runner " + runnerId, "open")).get(1, TimeUnit.SECONDS);
                }
            }
        }
    }

    private EventDO createEvent(Long eventId, String name, String status) {
        EventDO eventDo = new EventDO();
        eventDo.setId(eventId);
        eventDo.setName(name);
        eventDo.setStatus(status);
        return eventDo;
    }

    private MarketDO createMarket(Long eventId, Long marketId, String name, String status) {
        MarketDO marketDo = new MarketDO();
        marketDo.setId(marketId);
        marketDo.setEventId(eventId);
        marketDo.setName(name);
        marketDo.setStatus(status);
        return marketDo;
    }

    private RunnerDO createRunner(Long eventId, Long marketId, Long runnerId, String name, String status) {
        RunnerDO runnerDo = new RunnerDO();
        runnerDo.setId(runnerId);
        runnerDo.setEventId(eventId);
        runnerDo.setMarketId(marketId);
        runnerDo.setName(name);
        runnerDo.setStatus(status);
        return runnerDo;
    }

    private Long getUniqueId() {
        return random.nextLong();
    }

}

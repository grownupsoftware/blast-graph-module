package blast.graph.core.utils;

/**
 *
 * @author grant
 */
public interface ModificationWatcher {

    public void added(Object value);

    public void changed(Object value);

    public void removed(Object value);
}

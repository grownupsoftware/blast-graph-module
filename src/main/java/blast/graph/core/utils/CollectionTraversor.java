package blast.graph.core.utils;

import blast.exception.BlastException;
import blast.graph.core.dataobjects.PathDetails;
import blast.log.BlastLogger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 *
 * @author grant
 */
public class CollectionTraversor {

    protected final static BlastLogger logger = BlastLogger.createLogger();

    public static List<Map<String, Object>> findList(String path, List<Map<String, Object>> list) throws BlastException {
        return traverseUntilFinalList(path, list);
    }

    public static List<Map<String, Object>> findList(String path, Map<String, Object> data) throws BlastException {
        PathDetails[] pathDetails = PathDetails.splitPath(path);
        List<Map<String, Object>> resultList = null;
        for (int index = 0; index < pathDetails.length; index++) {
            if (pathDetails[index].getKeyField() == null) {
                resultList = (List<Map<String, Object>>) data.get(pathDetails[index].getCollection());
            } else {
                data = findRecord(index, pathDetails, data);
            }
        }
        return resultList == null ? new ArrayList<>() : resultList;

    }

    public static Map<String, Object> findRecord(String path, Map<String, Object> data) throws BlastException {
        PathDetails[] pathDetails = PathDetails.splitPath(path);
        for (int index = 0; index < pathDetails.length; index++) {
            data = findRecord(index, pathDetails, data);
        }
        return data;
    }

    public static Map<String, Object> findRecord(String path, List<Map<String, Object>> list) throws BlastException {
        return traverseUntilFinalRecord(path, list);
    }

    private static Map<String, Object> findRecordInList(String keyField, String keyValue, List<Map<String, Object>> list) throws BlastException {

        if (list != null) {
            for (Map<String, Object> record : list) {

                Object id = record.get(keyField);
                if (id != null) {
                    logger.debug("looking for: [{}] id is: [{}] are equal: [{}]", keyValue, id, matches(id, keyValue));
                    if (matches(id, keyValue)) {
                        logger.debug("found record");
                        return (Map<String, Object>) record;
                    }
                }
            }
        }
        throw new BlastException("failed to find record - field [" + keyField + " ]value [" + keyValue + "]");
    }

    private static Map<String, Object> findRecord(int index, PathDetails[] pathDetails, Map<String, Object> dataSubset) throws BlastException {
        List<Map<String, Object>> list = (List<Map<String, Object>>) dataSubset.get(pathDetails[index].getCollection());
        return findRecordInList(pathDetails[index].getKeyField(), pathDetails[index].getKeyValue(), list);
    }

    private static Map<String, Object> traverseUntilFinalRecord(String path, List<Map<String, Object>> list) throws BlastException {
        logger.debug("data: {}", list);
        PathDetails[] pathDetails = PathDetails.splitPath(path);
        Map<String, Object> data = findRecordInList(pathDetails[0].getKeyField(), pathDetails[0].getKeyValue(), list);
        for (int index = 1; index < pathDetails.length; index++) {
            data = findRecord(index, pathDetails, data);
        }
        return data;
    }

    private static List<Map<String, Object>> traverseUntilFinalList(String path, List<Map<String, Object>> list) throws BlastException {
        logger.debug("data: {}", list);
        PathDetails[] pathDetails = PathDetails.splitPath(path);
        logger.info("pathDetails: {}", Arrays.asList(pathDetails));

        List<Map<String, Object>> resultList = null;
        Map<String, Object> data = findRecordInList(pathDetails[0].getKeyField(), pathDetails[0].getKeyValue(), list);

        for (int index = 1; index < pathDetails.length; index++) {
            if (pathDetails[index].getKeyField() == null) {
                resultList = (List<Map<String, Object>>) data.get(pathDetails[index].getCollection());
            } else {
                data = findRecord(index, pathDetails, data);
            }
        }

        return resultList == null ? new ArrayList<>() : resultList;

    }

    private Map<String, Object> traverseUntilFinalRecord(String key, String path, Map<String, Object> data) throws BlastException {
        logger.debug("data: {}", data);

        if (path.isEmpty()) {
            return data;
        }
        if (key.endsWith(path)) {
            // if key =  markets/id:101/runners/id:103 and path = runners/id:103
            // then data is not actually hierarchal
            return data;
        }

        logger.info("path-> {}", path);
        PathDetails[] pathDetails = PathDetails.splitPath(path);
        for (int index = 0; index < pathDetails.length; index++) {
            data = findRecord(index, pathDetails, data);
        }
        return data;
    }

    public static boolean matches(Object one, String two) throws BlastException {
        switch (one.getClass().getCanonicalName()) {
            case "java.lang.Integer":
                return ((Integer) one).equals(Integer.valueOf(two));
            case "java.lang.Long":
                return ((Long) one).equals(Long.valueOf(two));
            case "java.lang.String":
                return ((String) one).equals(two);
            default:
                throw new BlastException("don't know how to compare values");
        }
    }

}

package blast.graph.core.utils;

import blast.exception.BlastException;
import static blast.graph.core.GraphCollection.COLLECTION_MASTER;
import com.tinkerpop.blueprints.Direction;
import com.tinkerpop.blueprints.Edge;
import com.tinkerpop.blueprints.Graph;
import com.tinkerpop.blueprints.Vertex;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author grant
 */
public class SchemaUtils {

    public static String dumpSchema(Graph graph) throws BlastException {
        return dumpSchema(graph, COLLECTION_MASTER);
    }

    public static String dumpSchema(Graph graph, String collectionName) throws BlastException {
        return GraphUtils.prettyPrint(getSchema(graph, collectionName));
    }

    public static Map<String, Object> getSchema(Graph graph) throws BlastException {
        return getSchema(graph, COLLECTION_MASTER);
    }

    public static Map<String, Object> getSchema(Graph graph, String collectionName) throws BlastException {
        boolean nameOnly = false;
        Map<String, Object> mainMap = new HashMap<>();

        Vertex vCollection = graph.getVertex(collectionName);
        Map<String, Object> collectionMap = new HashMap<>();

//        List<Map<String, Object>> childList = new ArrayList<>();
        Iterable<Edge> edges = vCollection.getEdges(Direction.OUT);
        for (Edge edge : edges) {
            Map<String, Object> childMap = new HashMap<>();
            Vertex child = edge.getVertex(Direction.IN);
            String name = child.getProperty("name");
            if (nameOnly) {
                childMap.put("name", child.getProperty("name"));
            } else {
                child.getPropertyKeys().forEach((property) -> {
                    childMap.put(property, child.getProperty(property));
                });
            }
            //childList.add(childMap);
            collectionMap.put(name, childMap);
        }
//        mainMap.put(collectionName, childList);
        mainMap.put(collectionName, collectionMap);

        return mainMap;

    }
}

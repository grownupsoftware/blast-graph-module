package blast.graph.core.utils;

/**
 *
 * @author grant
 */
public class AbstractModificationWatcher implements ModificationWatcher{

    @Override
    public void added(Object value) {
    }

    @Override
    public void changed(Object value) {
    }

    @Override
    public void removed(Object value) {
    }

}

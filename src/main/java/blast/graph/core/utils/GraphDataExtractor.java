package blast.graph.core.utils;

import blast.exception.BlastException;
import blast.graph.core.dataobjects.PathDetails;
import blast.graph.core.dataobjects.PathParameters;
import blast.server.BlastServer;
import static blast.graph.core.BlastGraph.ATTACH_MASTER;
import static blast.graph.core.BlastGraph.CLIENT_MASTER;
import blast.graph.core.GraphCollection;
import static blast.graph.core.GraphCollection.COLLECTION_DATA_EDGE;
import static blast.graph.core.GraphCollection.COLLECTION_EDGE_NAME;
import static blast.graph.core.GraphCollection.COLLECTION_MASTER;
import static blast.graph.core.GraphCollection.COLLECTION_PREFIX;
import blast.log.BlastLogger;
import blast.utils.BlastUtils;
import com.tinkerpop.blueprints.Direction;
import com.tinkerpop.blueprints.Edge;
import com.tinkerpop.blueprints.Graph;
import com.tinkerpop.blueprints.Vertex;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author grant
 */
public class GraphDataExtractor {

    protected static final BlastLogger logger = BlastLogger.createLogger();

    public static List<Map<String, Object>> getCollectionData(Graph graph, String key, PathParameters params) throws BlastException {
        Map<String, Object> data = getData(graph, COLLECTION_PREFIX + key, true, params);
        Object object = data.get(key);
        if (object == null) {
            return new ArrayList<>();
        }
        return (List<Map<String, Object>>) object;
    }

    public static <K> Map<String, Object> getData(Graph graph, K key, PathParameters params) throws BlastException {
        return getData(graph, key, false, params);
    }

    public static <K> Map<String, Object> getData(Graph graph, Vertex vertex, boolean isCollection, PathParameters params) {
        boolean includeFields = true;
        if (vertex.getId().toString().startsWith(COLLECTION_PREFIX) || vertex.getId().toString().equals(COLLECTION_MASTER)) {
            includeFields = false;
        }
        List<Map<String, Object>> newList = new ArrayList<>();
        Map<String, Object> mainMap = getChildData(vertex, includeFields, newList, params);
        if (isCollection) {
            mainMap.put(vertex.getId().toString().replace(COLLECTION_PREFIX, ""), newList);
        }

        return mainMap;

    }

    public static <K> Map<String, Object> getData(Graph graph, K key, boolean isCollection, PathParameters params) throws BlastException {
        Vertex vertex = graph.getVertex(key);
        if (vertex == null) {
            throw new BlastException("No record found: " + key.toString());
        }

        return getData(graph, vertex, isCollection, params);

    }

    private static Map<String, Object> getChildData(Vertex vertex, boolean includeFields, List<Map<String, Object>> childList, PathParameters params) {
        Map<String, Object> map = new HashMap<>();
        if (vertex == null) {
            return map;
        }

        if (includeFields && (params == null || (params.getFields() == null || params.getFields().isEmpty()))) {
            BlastUtils.safeStream(vertex.getPropertyKeys()).forEach((property) -> {
                map.put(property, vertex.getProperty(property));
            });
            // always add the key
            //FIXME id should be keyname
            map.put("id", vertex.getId());
        } else if (params != null && params.getFields() != null && !params.getFields().isEmpty()) {
            logger.debug("only include fields: {}", params.getFields());
            BlastUtils.safeStream(params.getFields()).forEach((property) -> {
                map.put(property, vertex.getProperty(property));
            });
            // always add the key
            //FIXME id should be keyname
            map.put("id", vertex.getId());

        }

        Map<String, List<Map<String, Object>>> edgeCollections = new HashMap<>();

        Iterable<Edge> edges = vertex.getEdges(Direction.OUT);

        for (Edge edge : edges) {
            Vertex child = edge.getVertex(Direction.IN);

            //String childName = child.getId().toString();
            boolean includeChildFields = true;
            switch (edge.getLabel()) {
                case COLLECTION_EDGE_NAME:
                    // strip 'collection-' from id
                    String childName = child.getId().toString().replace(COLLECTION_PREFIX, "");
                    includeChildFields = false;
                    List<Map<String, Object>> newList = edgeCollections.get(childName);
                    if (newList == null) {
                        newList = new ArrayList<>();
                        edgeCollections.put(childName, newList);
                    }

                    // vertex.getId().equals(COLLECTION_MASTER) ||
                    if (params == null || params.hasIncludeChildren()) {
                        childList.add(getChildData(child, includeChildFields, newList, params));
                    } else {
                        logger.debug("ignoring collection children: {} ", edge.getLabel());
                    }
                    break;
                case COLLECTION_DATA_EDGE:
                    childList.add(getChildData(child, includeChildFields, childList, params));
                    break;
                default:
                    if (params == null || params.hasIncludeChildren()) {
                        List<Map<String, Object>> edgeList = edgeCollections.get(edge.getLabel());
                        if (edgeList == null) {
                            edgeList = new ArrayList<>();
                            edgeCollections.put(edge.getLabel(), edgeList);
                        }

                        edgeList.add(getChildData(child, includeChildFields, edgeList, params));
                        map.put(edge.getLabel(), edgeList);
                    } else {
                        logger.debug("ignoring child: {} ", edge.getLabel());
                    }

            }
        }
        if (!includeFields) {
            BlastUtils.safeStream(edgeCollections.entrySet()).forEach(entry -> {
                map.put(entry.getKey(), entry.getValue());
            });
        }
        return map;
    }

    public static List<Map<String, Object>> getAllEdgeData(Vertex vertex, String edgeName, PathParameters params) {
        List<Map<String, Object>> list = new ArrayList<>();
        Iterable<Edge> edges = vertex.getEdges(Direction.OUT, edgeName);
        for (Edge edge : edges) {
            Vertex child = edge.getVertex(Direction.IN);
            list.add(getChildData(child, true, list, params));

        }
        return list;
    }

    public static PathDetails getLastPathDetail(String key) throws BlastException {
        PathDetails[] pathDetails = PathDetails.splitPath(key);
        return pathDetails[pathDetails.length - 1];
    }

    public static Object fetch(BlastServer blastServer, Graph graph, String key, PathParameters params) throws BlastException {

        PathDetails[] pathDetails = PathDetails.splitPath(key);
        if (pathDetails.length == 1) {
            // simple path - can only be 'root' or a 'collection'
            if (pathDetails[0].isRoot()) {
                return getData(graph, COLLECTION_MASTER, false, params);
            } else if (pathDetails[0].isCollectionOnly()) {
                return getCollectionData(graph, pathDetails[0].getCollection(), params);
            } else {
                if (pathDetails[0].getCollection().equals(CLIENT_MASTER) || pathDetails[0].getCollection().equals(ATTACH_MASTER)) {
                    // these are system collections
                    return getData(graph, pathDetails[0].getKeyValue(), false, params);
                } else {

                    if (pathDetails[0].getCollection() != null && pathDetails[0].getKeyField() != null) {
                        return getDataForId(blastServer, graph, pathDetails[0], params);
                    } else {
                        // should not get here - should be caught earlier
                        throw new BlastException("Must specify a collection");
                    }
                }
            }
        } else {
            // complex path - can be a 'collection' of a parent, or parent
            int index = pathDetails.length - 1;
            if (pathDetails[index].getKeyValue() != null) {
                // has a key, as keys are unique we can go and get data straight away
                return getDataForId(blastServer, graph, pathDetails[index], params);
            }
            // so it is a collection of a parent id e.g events/id:100/markets
            // need to get collection from parent
            return getDataForCollectionOfParent(blastServer, graph, pathDetails[index - 1], pathDetails[index].getCollection(), params);
        }
    }

    private static List<Map<String, Object>> getDataForCollectionOfParent(BlastServer blastServer, Graph graph, PathDetails pathDetails, String collection, PathParameters params) throws BlastException {

        GraphCollection graphCollection = GraphCollection.getCollection(blastServer, graph, pathDetails.getCollection());
        if (!pathDetails.getKeyField().equals(graphCollection.getKeyName())) {
            throw new BlastException("keyfield specified must match collections key field i.e. " + pathDetails.getKeyField() + " s/be " + graphCollection.getKeyName());
        }

        Object parentKeyValue = GraphUtils.getKeyValue(graphCollection.getKeyType(), pathDetails.getKeyValue());
        Vertex parent = graph.getVertex(parentKeyValue);

        return getAllEdgeData(parent, collection, params);
    }

    private static Map<String, Object> getDataForId(BlastServer blastServer, Graph graph, PathDetails pathDetails, PathParameters params) throws BlastException {
        return getDataForId(blastServer, graph, pathDetails.getCollection(), pathDetails.getKeyField(), pathDetails.getKeyValue(), params);
    }

    private static Map<String, Object> getDataForId(BlastServer blastServer, Graph graph, String collection, String keyField, String keyValue, PathParameters params) throws BlastException {
        GraphCollection graphCollection = GraphCollection.getCollection(blastServer, graph, collection);
        if (!keyField.equals(graphCollection.getKeyName())) {
            throw new BlastException("keyfield specified must match collections key field i.e. " + keyField + " s/be " + graphCollection.getKeyName());
        }
        Object convertedKeyValue = GraphUtils.getKeyValue(graphCollection.getKeyType(), keyValue);
        return getData(graph, convertedKeyValue, false, params);
    }
}

/*
 * Grownup Software Limited.
 */
package blast.graph.core;

import blast.client.BlastServerClient;
import blast.doc.DocField;
import blast.exception.BlastException;
import blast.server.BlastServer;
import blast.graph.core.builders.CollectionBuilder;
import blast.graph.core.dataobjects.PathParameters;
import com.tinkerpop.blueprints.Graph;
import java.util.List;
import java.util.Map;

/**
 *
 * @author grant
 */
public interface BlastGraph {

    @DocField(description = "Collection name from which all connected clientes are attached to")
    public static final String CLIENT_MASTER = "client-master";

    @DocField(description = "Edge name between client-master and client vertex")
    public static final String CLIENT_MASTER_EDGE = "clients";

    @DocField(description = "All attachments are connected to the attach-master")
    public static final String ATTACH_MASTER = "attach-master";

    @DocField(description = "The audit vertex - not implemented")
    public static final String AUDIT_MASTER = "audit";

    @DocField(description = "Protected internal collections")
    public static final String[] INTERNAL_COLLECTIONS = new String[]{
        CLIENT_MASTER,
        ATTACH_MASTER,
        AUDIT_MASTER
    };

    public Graph getGraph();

    /**
     * Gets root data
     *
     * @param params apply path parameters
     * @return map by collection of all collections
     * @throws BlastException
     */
    public Map<String, Object> getRootData(PathParameters params) throws BlastException;

    /**
     * Get all data for a collection
     *
     * @param collectionName the name of the col
     * @param params apply path parameters
     * @return all entities for this collection
     * @throws BlastException
     */
    public List<Map<String, Object>> getCollectionData(String collectionName, PathParameters params) throws BlastException;

    /**
     * Gets data for a specific key - not collection related
     *
     * @param <K> The type of Key e.g. String, Long, Integer
     * @param key the value of the key
     * @param params
     * @return the data for this entity
     * @throws BlastException
     */
    public <K> Map<String, Object> getData(K key, PathParameters params) throws BlastException;

    /**
     * Removes a collection
     *
     * @param collectionName The name of the collection to remove - also removes
     * all underling data
     */
    public void removeCollection(String collectionName);

    /**
     * Schema dump of collections and their settings - no collection data is
     * returned
     *
     * @return list of graph collection summary
     * @throws BlastException
     */
    public List<GraphCollection> getCollections() throws BlastException;

    /**
     * Returns a builder for collection creation
     *
     * @return
     */
    public CollectionBuilder createCollection();

    /**
     * Creates the collection
     *
     * @param <T> Type of underlying data class
     * @param <K> Type of key e.g. String, Long, Integer
     * @param collectionName The name of the collection to create
     * @param keyname The key name i.e. the name of the unique identifier -
     * normally 'id'
     * @param clazz The Data Class i.e. a POJO that is associated with this
     * collection
     * @param keyType The class of the key e.g. String,Long,Integer
     * @param includeFields only include fields when building the collection
     * @param excludeFields include all fields from POJO except these
     * @return A new Graph Collection
     * @throws BlastException
     */
    public <T, K> GraphCollection createCollection(String collectionName, String keyname, Class<T> clazz, Class<K> keyType, String[] includeFields, String[] excludeFields) throws BlastException;

    /**
     * Get the data for an entity but only include children specified by the
     * 'includeRelationships'
     *
     * @param <K> The Type of key
     * @param key The value of the Key
     * @param includeRelationships An array of relationships (edges) to be
     * returned
     * @return Map of the data for this entity
     * @throws BlastException
     */
    public <K> Map<String, Object> get(K key, String... includeRelationships) throws BlastException;

//    /**
//     * Remove an entity
//     *
//     * @param <K> The Type of key
//     * @param key The value of the Key
//     */
//    public <K> void remove(K key);
    /**
     * Starting from a entity id bubble up through all parent relationships
     * (edges) until the parent collection is reached
     *
     * @param <K> The Type of key
     * @param key The value of the Key
     * @throws BlastException
     */
    public <K> void bubbleUp(K key) throws BlastException;

    /**
     *
     * @param client The blast server client
     * @param key follows format of "collection" or
     * "collection/keyfield:keyvalue" or
     * "collection/keyfield:keyvalue/childcollection"
     * @param params additional parameters
     * @return Either Map<String,Object> or List<Map<String,Object>>
     * @throws BlastException
     */
    public Object fetch(BlastServerClient client, String key, PathParameters params) throws BlastException;

    public void closeClient(BlastServerClient client);

    public void createClient(BlastServerClient client);

    /**
     *
     * @param client the blast server client
     * @param key follows format of "collection" or
     * "collection/keyfield:keyvalue" or
     * "collection/keyfield:keyvalue/childcollection"
     * @param attachmentId client side unique identifier for this collection
     * @param params additional parameters
     * @return Either Map<String,Object> or List<Map<String,Object>>
     * @throws BlastException
     */
    public Object attachTo(BlastServerClient client, String attachmentId, String key, PathParameters params) throws BlastException;

    /**
     * No longer attach to this key i.e. no longer receive any updates
     *
     * @param client
     * @param attachmentId unique attachment Id
     * @throws BlastException
     */
    public void detachFrom(BlastServerClient client, String attachmentId) throws BlastException;

    public void detachFromAll(BlastServerClient client) throws BlastException;

    public List<Map<String, Object>> getClientAttachments(BlastServerClient client) throws BlastException;

    public void add(String key, Map<String, Object> data) throws BlastException;

    public void update(String key, Map<String, Object> data) throws BlastException;

    public void remove(String key) throws BlastException;

    public void setBlastServer(BlastServer blastServer);

}

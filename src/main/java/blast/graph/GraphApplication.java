/*
 * Grownup Software Limited.
 */
package blast.graph;

import blast.Blast;
import blast.exception.BlastException;
import blast.server.BlastServer;
import blast.graph.core.BlastGraph;
import blast.graph.core.BlastGraphImpl;
import blast.graph.example.SportsBookGraphBuilder;
import blast.graph.module.GraphModule;
import blast.log.BlastLogger;
import blast.vertx.engine.VertxEngine;
import io.vertx.core.Vertx;
import io.vertx.core.VertxOptions;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.handler.StaticHandler;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.Options;

/**
 *
 * @author grant
 */
public class GraphApplication {

    private final BlastLogger logger = BlastLogger.createLogger();

    private void start(int port) {
        //startStaticWebServer(port);
        startBlast(port);
    }

    private void startBlast(int port) {
        try {

            // create a new Blast Graph
            BlastGraph blastGraph = new BlastGraphImpl();

            // create graphmodule with the graph
            GraphModule graphModule = new GraphModule(blastGraph);

            BlastServer blast = Blast.blast(new VertxEngine(), graphModule);

            // add server to blast graph
            blastGraph.setBlastServer(blast);

            // Build a graph repository
            SportsBookGraphBuilder graphBuilder = new SportsBookGraphBuilder(graphModule.getGraph());
            graphBuilder.buildGraph();

            blast.startup();

        } catch (BlastException ex) {
            Blast.logger.warn("Can't start Blast!", ex);
        }

    }

    /**
     * Start a web server that delivers static content. This is not related in
     * anyway with blast, it is simply an embedded web server that is able to
     * serve out HTML code. We are using VertX, but it could just as well be
     * Tomcat, Undertow etc
     *
     * @param port
     */
    private void startStaticWebServer(int port) {
        Vertx vertx = Vertx.vertx(new VertxOptions().setWorkerPoolSize(5));

        // this will not work as resources bundled as a jar
        // System.setProperty("vertx.disableFileCaching", "true");
        Router router = Router.router(vertx);

//        StaticHandler staticHandler = StaticHandler.create("webapp");
        StaticHandler staticHandler = StaticHandler.create("../../../src/main/resources/webapp");
        staticHandler.setIndexPage("index.html");
        staticHandler.setCachingEnabled(false);

        router.route("/*").handler(staticHandler);

        logger.info("Website started on http://localhost:{}/", port);
        vertx.createHttpServer().requestHandler(router::accept).listen(port);
    }

    /**
     * Main entry point
     *
     * @param args
     * @throws Exception
     */
    public static void main(String... args) throws Exception {
        Options options = new Options();
        options.addOption("port", true, "port for the website");

        int port = 8100;
        CommandLineParser parser = new DefaultParser();
        CommandLine commandLine = parser.parse(options, args);
        String portStr = commandLine.getOptionValue("port");
        if (portStr != null && !portStr.isEmpty()) {
            port = Integer.valueOf(portStr);
        }

        GraphApplication application = new GraphApplication();
        application.start(port);
    }

}

package blast.graph.example;

import blast.exception.BlastException;
import blast.graph.core.BlastGraph;
import blast.graph.core.GraphCollection;
import blast.graph.example.model.EventDO;
import blast.graph.example.model.MarketDO;
import blast.graph.example.model.RunnerDO;
import blast.log.BlastLogger;
import java.util.concurrent.atomic.AtomicLong;

/**
 *
 * @author grant
 */
public class SportsBookGraphBuilder {

    public static final Integer NUMBER_EVENTS = 2;
    public static final Integer NUMBER_MARKETS_PER_EVENT = 3;
    public static final Integer NUMBER_RUNNERS_PER_MARKET = 5;

    private static final BlastLogger logger = BlastLogger.createLogger();
    private final AtomicLong uniqueId = new AtomicLong(100);
    private BlastGraph theBlastGraph;

    public SportsBookGraphBuilder(BlastGraph blastGraph) {
        this.theBlastGraph = blastGraph;
    }

    public void buildGraph() throws BlastException {

        // Step 1 ---- create the collections
        // create events collection
        GraphCollection<EventDO, Long> events = theBlastGraph.createCollection()
                .name("events")
                .keyfield("id")
                .dataclass(EventDO.class)
                .excludeFields("markets")
                .build();

        // create markets collection
        GraphCollection<MarketDO, Long> markets = theBlastGraph.createCollection()
                .name("markets")
                .keyfield("id")
                .dataclass(MarketDO.class)
                //.onlyFields("name")
                .excludeFields("runners")
                .build();

        // create runners collection
        GraphCollection<RunnerDO, Long> runners = theBlastGraph.createCollection()
                .name("runners")
                .keyfield("id")
                .dataclass(RunnerDO.class)
                //.onlyFields("name")
                .excludeFields("prices")
                .build();

        // Step 2 ---- create relationships between collections
        // event 1-* markets.eventId
        markets.linkedTo(events).using("eventId");
        runners.linkedTo(markets).using("marketId");

        // Step 3 - log the schema
        //theBlastGraph.dumpSchema();
        // Step 4 - create some test data
        createData(NUMBER_EVENTS, NUMBER_MARKETS_PER_EVENT, NUMBER_RUNNERS_PER_MARKET, events, markets, runners);

    }

    private void createData(int numberEvents, int numMarketsPerEvent, int numRunnersPerMarket, GraphCollection eventCollection, GraphCollection marketCollection, GraphCollection runnerCollection) throws BlastException {
        logger.info("Generating data. {} events each with {} markets each with {} runners", numberEvents, numMarketsPerEvent, numRunnersPerMarket);
        for (int eventCnt = 0; eventCnt < numberEvents; eventCnt++) {
            Long eventId = getUniqueId();
            eventCollection.add(createEvent(eventId, "event " + eventId, "open"));
            for (int marketCnt = 0; marketCnt < numMarketsPerEvent; marketCnt++) {
                Long marketId = getUniqueId();
                marketCollection.add(createMarket(marketId, eventId, "market " + marketId, "open"));
                for (int runnerCnt = 0; runnerCnt < numRunnersPerMarket; runnerCnt++) {
                    Long runnerId = getUniqueId();
                    runnerCollection.add(createRunner(runnerId, eventId, marketId, "runner " + runnerId, "open"));
                }
            }
        }
    }

    private EventDO createEvent(Long id, String name, String status) {
        EventDO eventDo = new EventDO();
        eventDo.setId(id);
        eventDo.setName(name);
        eventDo.setStatus(status);
        return eventDo;
    }

    private MarketDO createMarket(Long id, Long eventId, String name, String status) {
        MarketDO marketDo = new MarketDO();
        marketDo.setId(id);
        marketDo.setEventId(eventId);
        marketDo.setName(name);
        marketDo.setStatus(status);
        return marketDo;
    }

    private RunnerDO createRunner(Long id, Long eventId, Long marketId, String name, String status) {
        RunnerDO runnerDo = new RunnerDO();
        runnerDo.setId(id);
        runnerDo.setEventId(eventId);
        runnerDo.setMarketId(marketId);
        runnerDo.setName(name);
        runnerDo.setStatus(status);
        return runnerDo;
    }

    private Long getUniqueId() {
        return uniqueId.getAndIncrement();
    }

}

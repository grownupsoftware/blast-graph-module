package blast.graph.example.model;

import blast.doc.DocClass;
import blast.doc.DocField;
import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author grant
 */
@DocClass(description = "Market Information")
public class MarketDO {

    @DocField(description = "Id of market")
    private Long id;

    @DocField(description = "Id of event")
    private Long eventId;

    @DocField(description = "Name of market")
    private String name;

    @DocField(description = "The status of this market")
    private String status;

    @DocField(description = "The runners for this market")
    private Set<RunnerDO> runners = new HashSet<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getEventId() {
        return eventId;
    }

    public void setEventId(Long eventId) {
        this.eventId = eventId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Set<RunnerDO> getRunners() {
        return runners;
    }

    public void setRunners(Set<RunnerDO> runners) {
        this.runners = runners;
    }

    @Override
    public String toString() {
        return "MarketDO{" + "id=" + id + ", eventId=" + eventId + ", name=" + name + ", status=" + status + ", runners=" + runners + '}';
    }

}

package blast.graph.example.model;

import blast.doc.DocClass;
import blast.doc.DocField;
import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author grant
 */
@DocClass(description = "Market Information")
public class RunnerDO {

    @DocField(description = "Id of runner")
    private Long id;

    @DocField(description = "Id of event")
    private Long eventId;

    @DocField(description = "Id of market")
    private Long marketId;

    @DocField(description = "Name of runner")
    private String name;

    @DocField(description = "The status of this runner")
    private String status;

    @DocField(description = "The prices for this runner")
    private Set<PriceDO> prices = new HashSet<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getEventId() {
        return eventId;
    }

    public void setEventId(Long eventId) {
        this.eventId = eventId;
    }

    public Long getMarketId() {
        return marketId;
    }

    public void setMarketId(Long marketId) {
        this.marketId = marketId;
    }

    public Set<PriceDO> getPrices() {
        return prices;
    }

    public void setPrices(Set<PriceDO> prices) {
        this.prices = prices;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}

package blast.graph.example.model;

import blast.doc.DocClass;
import blast.doc.DocField;
import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author grant
 */
@DocClass(description = "Event Information")
public class EventDO {

    @DocField(description = "Id of event")
    private Long id;

    @DocField(description = "Name of event")
    private String name;

    @DocField(description = "The markets for this event")
    private Set<MarketDO> markets = new HashSet<>();

    @DocField(description = "The status of this event")
    private String status;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<MarketDO> getMarkets() {
        return markets;
    }

    public void setMarkets(Set<MarketDO> markets) {
        this.markets = markets;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "EventDO{" + "id=" + id + ", name=" + name + ", markets=" + markets + ", status=" + status + '}';
    }

}

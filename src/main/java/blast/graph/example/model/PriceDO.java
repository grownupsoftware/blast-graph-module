package blast.graph.example.model;

import blast.doc.DocClass;
import blast.doc.DocField;
import java.math.BigDecimal;

/**
 *
 * @author grant
 */
@DocClass(description = "Price Information")
public class PriceDO {

    @DocField(description = "Id of runner")
    private String id;

    @DocField(description = "Id of runner")
    private BigDecimal price;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "PriceDO{" + "id=" + id + ", price=" + price + '}';
    }

}

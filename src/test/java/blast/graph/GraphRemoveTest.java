package blast.graph;

import blast.exception.BlastException;
import blast.graph.core.utils.AbstractModificationWatcher;
import blast.graph.core.utils.CollectionTraversor;
import static blast.graph.core.utils.GraphUtils.prettyPrint;
import blast.graph.example.model.RunnerDO;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import org.junit.Test;

/**
 *
 * @author grant
 */
public class GraphRemoveTest extends AbstractGraphTest {

    @Test
    public void test_remove() {
        logger.info("-- Testing Remove");
        try {

            Map<String, Object> collection = new HashMap<>();
            getClient().attach("markets/id:101", collection, new AbstractModificationWatcher() {
                @Override
                public void removed(Object value) {
                    logger.info("collection removed: {}", value);
                }
            }).get(1, TimeUnit.SECONDS);
//            getClient().attach("markets/id:101/runners", collection).get(1, TimeUnit.SECONDS);

            assertTrue("collection should not be empty", !collection.isEmpty());

            createRunner(1003l);
            Map<String, Object> runnerObject = new HashMap<>();

            getClient().attach("runners/id:1003", runnerObject, new AbstractModificationWatcher() {
                @Override
                public void removed(Object value) {
                    logger.info("runner collection removed: {}", value);
                }
            }).get(1, TimeUnit.SECONDS);

            List<Map<String, Object>> clientAttachments = getClient().getAttachments().get(1, TimeUnit.SECONDS);
            logger.info("Before Client Attachments : {}", prettyPrint(clientAttachments));

            logger.info("before records:{}", prettyPrint(getClient().fetch("markets/id:101/runners").get(1, TimeUnit.SECONDS)));
            logger.info("before collection: {}", prettyPrint(collection));

            Map<String, Object> record = CollectionTraversor.findRecord("runners/id:1003", collection);
            logger.info("new record: {}", prettyPrint(record));
            assertNotNull("Should find newly added runner", record);
            assertTrue("name should have changed", record.get("name").equals("new name"));
            assertTrue("status should have changed", record.get("status").equals("open"));

            getClient().remove("runners/id:1003").get(1, TimeUnit.SECONDS);

            logger.info("after records:{}", prettyPrint(getClient().fetch("markets/id:101/runners").get(1, TimeUnit.SECONDS)));

            clientAttachments = getClient().getAttachments().get(1, TimeUnit.SECONDS);
            logger.info("After Client Attachments : {}", prettyPrint(clientAttachments));

            try {
                logger.info("after collection: {}", prettyPrint(collection));
                record = CollectionTraversor.findRecord("runners/id:1003", collection);
                logger.info("record: {}", record);
                fail("should not get here");
            } catch (BlastException e) {
                assertTrue("Record should not exist", e.getMessage().startsWith("failed to find record"));
            }

        } catch (InterruptedException | ExecutionException | TimeoutException | BlastException ex) {
            logger.error("error", ex);
            fail(ex.getMessage());
        }
    }

    private void createRunner(Long runnerId) throws InterruptedException, ExecutionException, TimeoutException, BlastException {
        // add a runner
        RunnerDO runnerDo = createRunner(100l, 101l, runnerId, "new name", "open");
        CompletableFuture<Boolean> future = getClient().add("runners", runnerDo);

        Boolean result = future.get(1, TimeUnit.SECONDS);

        logger.info("Record added: {}", result);

        assertTrue("record should have been added", result);

    }
}

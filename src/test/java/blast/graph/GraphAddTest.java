package blast.graph;

import blast.exception.BlastException;
import blast.graph.core.utils.CollectionTraversor;
import static blast.graph.core.utils.GraphUtils.prettyPrint;
import blast.graph.example.model.RunnerDO;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import org.junit.Test;

/**
 *
 * @author grant
 */
public class GraphAddTest extends AbstractGraphTest {

    @Test
    public void test_add_to_object() {
        logger.info("-- Testing Add");
        try {

            Map<String, Object> collection = new HashMap<>();
            getClient().attach("markets/id:101", collection).get(1, TimeUnit.SECONDS);
            //logger.info("collection: {}", prettyPrint(collection));

            assertTrue("collection should not be empty", !collection.isEmpty());

            createRunner(999l);

            Map<String, Object> record = CollectionTraversor.findRecord("runners/id:999", collection);
            logger.info("new record: {}", prettyPrint(record));
            assertNotNull("Should find newly added runner", record);
            assertTrue("name should have changed", record.get("name").equals("new name"));
            assertTrue("status should have changed", record.get("status").equals("open"));

        } catch (InterruptedException | ExecutionException | TimeoutException | BlastException ex) {
            logger.error("error", ex);
            fail(ex.getMessage());
        }
    }

    @Test
    public void test_add_to_list() {
        logger.info("-- Testing Add");
        try {

            List<Map<String, Object>> collection = new ArrayList<>();
            getClient().attach("markets/id:101/runners", collection).get(1, TimeUnit.SECONDS);
            //logger.info("collection: {}", prettyPrint(collection));

            assertTrue("collection should not be empty", !collection.isEmpty());

            createRunner(1001l);

            Map<String, Object> record = CollectionTraversor.findRecord("runners/id:1001", collection);
            logger.info("new record: {}", prettyPrint(record));
            assertNotNull("Should find newly added runner", record);
            assertTrue("name should have changed", record.get("name").equals("new name"));
            assertTrue("status should have changed", record.get("status").equals("open"));

        } catch (InterruptedException | ExecutionException | TimeoutException | BlastException ex) {
            logger.error("error", ex);
            fail(ex.getMessage());
        }
    }

    private void createRunner(Long runnerId) throws InterruptedException, ExecutionException, TimeoutException, BlastException {
        // add a runner
        RunnerDO runnerDo = createRunner(100l, 101l, runnerId, "new name", "open");
        CompletableFuture<Boolean> future = getClient().add("runners", runnerDo);

        Boolean result = future.get(1, TimeUnit.SECONDS);

        logger.info("Record added: {}", result);

        assertTrue("record should have been added", result);

    }
}

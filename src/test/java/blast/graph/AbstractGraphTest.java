package blast.graph;

import blast.Blast;
import blast.exception.BlastException;
import blast.graph.client.GraphExternalClient;
import blast.graph.core.BlastGraph;
import blast.graph.core.BlastGraphImpl;
import blast.graph.example.SportsBookGraphBuilder;
import blast.graph.example.model.RunnerDO;
import blast.graph.module.GraphModule;
import blast.log.BlastLogger;
import blast.server.BlastServer;
import blast.utils.BlastUtils;
import blast.vertx.engine.VertxEngine;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutionException;
import org.junit.AfterClass;
import org.junit.BeforeClass;

/**
 *
 * @author grant
 */
public class AbstractGraphTest {

    protected static final BlastLogger logger = BlastLogger.createLogger();

    private static BlastServer theBlastServer = null;
    private static int thePort;
    protected static CountDownLatch theMessageLatch;
    public static ObjectMapper theObjectMapper;
    private static GraphExternalClient theExternalClient = null;
    private static boolean logIncoming = false;

    @BeforeClass
    public static void setup() {

        try {
            System.setProperty("blast-server.config", "blast-test-server.json");
            theBlastServer = startBlastServer();
            theObjectMapper = theBlastServer.getObjectMapper();

            thePort = theBlastServer.getProperties().getEndpoint().getPort();

            theExternalClient = new GraphExternalClient(getTestWsEndpoint());

            // wait for connection
            theExternalClient.connect().get();

        } catch (InterruptedException | ExecutionException | BlastException ex) {
            logger.error("Failed to start the server", ex);
            sleep();
        }
    }

    @AfterClass
    public static void shutdown() {
        if (theExternalClient != null) {
            theExternalClient.close();
        }
        sleep();
        if (theBlastServer != null) {
            // sleep - sometimes I miss log messages
            // sleep - if external client is still closing and blast closes then I get ConcurrentModificationException
            sleep();
            theBlastServer.shutdown();
        }
    }

    public GraphExternalClient getClient() {
        return theExternalClient;
    }

    public ObjectMapper getObjectMapper() {
        return theObjectMapper;
    }

    protected static URI getTestWsEndpoint() throws BlastException {
        try {
            return new URI("ws://127.0.0.1:" + thePort + "/blast");
            // use below to test connection errors
            // return new URI("ws://127.0.0.1:8081/blast");
        } catch (URISyntaxException ex) {
            logger.error("URI format exception", ex);
            throw new BlastException("URI format exception", ex);
        }
    }

    public static BlastServer startBlastServer() throws BlastException {
        try {

            BlastGraph blastGraph = new BlastGraphImpl();
            GraphModule graphModule = new GraphModule(blastGraph);
            
            BlastServer blast = Blast.blast(new VertxEngine(), new blast.module.ping.PingModule(), new blast.module.echo.EchoModule(), graphModule);

            // add server to blast graph
            blastGraph.setBlastServer(blast);
            
            // Build a graph repository
            SportsBookGraphBuilder graphBuilder = new SportsBookGraphBuilder(graphModule.getGraph());
            graphBuilder.buildGraph();

            logger.info("starting with properties: {}", blast.getProperties());
            blast.startup();

            // give the server a chance to start
            sleep();

            return blast;
        } catch (BlastException ex) {
            logger.error("Can't start Blast!", ex);
            throw ex;
        }

    }

    // I find that when errors occur I can miss the last log message - so I sleep to ensure I get it
    public static void sleep() {
        BlastUtils.sleep(1000);
    }

    protected RunnerDO createRunner(Long eventId, Long marketId, Long id, String name, String status) {
        RunnerDO runnerDo = new RunnerDO();
        runnerDo.setId(id);
        runnerDo.setEventId(eventId);
        runnerDo.setMarketId(marketId);
        runnerDo.setName(name);
        runnerDo.setStatus(status);
        return runnerDo;
    }

}

package blast.graph;

import blast.exception.BlastException;
import blast.graph.core.utils.AbstractModificationWatcher;
import blast.graph.core.utils.CollectionTraversor;
import static blast.graph.core.utils.GraphUtils.prettyPrint;
import blast.graph.example.model.RunnerDO;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import org.junit.Test;

/**
 *
 * @author grant
 */
public class GraphUpdateTest extends AbstractGraphTest {

    @Test
    public void test_schema_test() {
        logger.info("-- Testing Schema Fetch");
        try {
            Map<String, Object> schema = getClient().getSchema().get(1, TimeUnit.SECONDS);
            //logger.info("schema: {}", prettyPrint(schema));

            assertTrue("schema should not be empty", !schema.isEmpty());

        } catch (InterruptedException | ExecutionException | TimeoutException | BlastException ex) {
            logger.error("error", ex);
            fail(ex.getMessage());
        }
    }

    @Test
    public void test_simple_fetch_root() {
        logger.info("-- Testing Simple Fetch root");
        try {
            Map<String, Object> data = getClient().fetch("").get(1, TimeUnit.SECONDS);
            logger.info("data: {}", prettyPrint(data));

            assertTrue("data should not be empty", !data.isEmpty());

        } catch (InterruptedException | ExecutionException | TimeoutException | BlastException ex) {
            logger.error("error", ex);
            fail(ex.getMessage());
        }
    }

    @Test
    public void test_simple_fetch() {
        logger.info("-- Testing Simple Fetch");
        try {
            Map<String, Object> data = getClient().fetch("markets/id:101/runners/id:103").get(1, TimeUnit.SECONDS);
            //logger.info("data: {}", prettyPrint(data));

            assertTrue("data should not be empty", !data.isEmpty());

        } catch (InterruptedException | ExecutionException | TimeoutException | BlastException ex) {
            logger.error("error", ex);
            fail(ex.getMessage());
        }
    }

    @Test
    public void test_simple_fetch_as_list() {
        logger.info("-- Testing Fetch result as List");
        try {
            List<Map<String, Object>> data = getClient().fetchAsList("markets/id:101/runners").get(1, TimeUnit.SECONDS);

            //logger.info("data: {}", prettyPrint(data));
            assertTrue("data should not be empty", !data.isEmpty());
        } catch (InterruptedException | ExecutionException | TimeoutException | BlastException ex) {
            logger.error("error", ex);
            fail(ex.getMessage());
        }
    }

    @Test
    public void test_attach_object() {
        logger.info("-- Testing Simple Attach");
        try {
            Map<String, Object> data = new HashMap<>();

            // attach data object to record id:103
            getClient().attach("markets/id:101/runners/id:103", data).get(1, TimeUnit.SECONDS);

            //logger.info("data: {}", prettyPrint(data));
            assertTrue("data should not be empty", !data.isEmpty());

        } catch (InterruptedException | ExecutionException | TimeoutException | BlastException ex) {
            logger.error("error", ex);
            fail(ex.getMessage());
        }
    }

    @Test
    public void test_attach_list() {
        logger.info("-- Testing Simple List Attach");
        try {
            List<Map<String, Object>> data = new ArrayList<>();

            getClient().attach("markets/id:101/runners", data).get(1, TimeUnit.SECONDS);

            //logger.info("data: {}", prettyPrint(data));
            assertTrue("data should not be empty", !data.isEmpty());

        } catch (InterruptedException | ExecutionException | TimeoutException | BlastException ex) {
            logger.error("error", ex);
            fail(ex.getMessage());
        }
    }

    @Test
    public void test_update_no_attachments() {
        logger.info("-- Testing Update - No Attachments");
        try {

            // Create a runner object - changing name and status
            RunnerDO runnerDo = createRunner(100l, 101l, 103l, "new name", "open");
            CompletableFuture<Boolean> future = getClient().update("runners/id:103", runnerDo);

            Boolean result = future.get(1, TimeUnit.SECONDS);

            //logger.info("Record Updated: {}", result);
            assertTrue("record should have been updated", result);

        } catch (InterruptedException | ExecutionException | TimeoutException | BlastException ex) {
            logger.error("error", ex);
            fail(ex.getMessage());
        }
    }

    @Test
    public void test_update_with_attachments_list_object() {
        logger.info("-- Testing Update - List Attachment");
        try {
            List<Map<String, Object>> collection = new ArrayList<>();

            getClient().attach("runners", collection).get(1, TimeUnit.SECONDS);
            //logger.info("After attach : {}", prettyPrint(collection));

//            Map<String, Object> root = getClient().fetchRoot().get(1, TimeUnit.SECONDS);
//            logger.info("Root : {}", prettyPrint(root));
            //List<Map<String, Object>> attachments = getClient().fetchAsList("attach-master").get(1, TimeUnit.SECONDS);
            //logger.info("Attachments : {}", prettyPrint(attachments));
            updateRunner(1, "fred");

            Map<String, Object> record = CollectionTraversor.findRecord("runners/id:103", collection);
            assertTrue("name should have changed", record.get("name").equals("new name: 1"));
            assertTrue("status should have changed", record.get("status").equals("fred"));

            // logger.info("Updated Collection : {}", prettyPrint(collection));
        } catch (InterruptedException | ExecutionException | TimeoutException | BlastException ex) {
            logger.error("error", ex);
            fail(ex.getMessage());
        }
    }

    @Test
    public void test_attach_detach() {
        logger.info("-- Testing Update - Simple Object Attachments");
        try {
            Map<String, Object> collectionOne = new HashMap<>();
            String attachmentId = getClient().attach("markets/id:101/runners/id:103", collectionOne).get(1, TimeUnit.SECONDS);

            List<Map<String, Object>> clientAttachments = getClient().getAttachments().get(1, TimeUnit.SECONDS);
            logger.info("Before Client Attachments : {}", prettyPrint(clientAttachments));

            List<Map<String, Object>> attachments = getClient().fetchAsList("attach-master").get(1, TimeUnit.SECONDS);
            logger.info("Before Detach : {}", prettyPrint(attachments));

            getClient().detach(attachmentId).get(1, TimeUnit.SECONDS);
            //getClient().detachAll().get(1, TimeUnit.SECONDS);

            attachments = getClient().fetchAsList("attach-master").get(1, TimeUnit.SECONDS);
            logger.info("After Detach : {}", prettyPrint(attachments));

            clientAttachments = getClient().getAttachments().get(1, TimeUnit.SECONDS);
            logger.info("After Client Attachments : {}", prettyPrint(clientAttachments));

        } catch (InterruptedException | ExecutionException | TimeoutException | BlastException ex) {
            logger.error("error", ex);
            fail(ex.getMessage());
        }

    }

    @Test
    public void test_update_with_attachments_single_object() {
        logger.info("-- Testing Update - Simple Object Attachments");
        try {

            // detach from all collections
            getClient().detachAll().get(1, TimeUnit.SECONDS);

            // create a collection
            Map<String, Object> collectionOne = new HashMap<>();
            Map<String, Object> collectionTwo = new HashMap<>();
            Map<String, Object> collectionThree = new HashMap<>();

            // attach collection to record id:103
            // works
            CompletableFuture<String> future1 = getClient().attach("markets/id:101/runners/id:103", collectionOne, new AbstractModificationWatcher() {
                @Override
                public void changed(Object value) {
                    logger.info("CollectionOne -> changed: {}", value);
                }
            });
            String attachmentId_One = future1.get(1, TimeUnit.SECONDS);

            CompletableFuture<String> future2 = getClient().attach("runners/id:103", collectionTwo, new AbstractModificationWatcher() {
                @Override
                public void changed(Object value) {
                    logger.info("CollectionTwo -> changed: {}", value);
                }
            });
            String attachmentId_Two = future2.get(1, TimeUnit.SECONDS);

            // attach to parent of a child that is updated
            CompletableFuture<String> future3 = getClient().attach("markets/id:101", collectionThree, new AbstractModificationWatcher() {
                @Override
                public void changed(Object value) {
                    logger.info("CollectionThree -> changed: {}", value);
                }
            });
            String attachmentId_Three = future3.get(1, TimeUnit.SECONDS);
            List<Map<String, Object>> attachments = getClient().fetchAsList("attach-master").get(1, TimeUnit.SECONDS);
            logger.info("Attachments : {}", prettyPrint(attachments));

            updateRunner(2, "joe");

            //logger.info("Updated Collection One: {}", prettyPrint(collectionOne));
            assertTrue("name should have changed", collectionOne.get("name").equals("new name: 2"));
            assertTrue("status should have changed", collectionOne.get("status").equals("joe"));

            //logger.info("Updated Collection Two: {}", prettyPrint(collectionTwo));
            assertTrue("name should have changed", collectionTwo.get("name").equals("new name: 2"));
            assertTrue("status should have changed", collectionTwo.get("status").equals("joe"));

            //logger.info("Updated Collection Three: {}", prettyPrint(collectionThree));
            Map<String, Object> record = CollectionTraversor.findRecord("runners/id:103", collectionThree);
            assertTrue("name should have changed", record.get("name").equals("new name: 2"));
            assertTrue("status should have changed", record.get("status").equals("joe"));
        } catch (InterruptedException | ExecutionException | TimeoutException | BlastException ex) {
            logger.error("error", ex);
            fail(ex.getMessage());
        }
    }

    @Test
    public void test_update_with_attachments_on_runners() {
        logger.info("-- Testing Update - Simple Object Attachments");
        try {

            // create a collection
            List<Map<String, Object>> collectionOne = new ArrayList<>();

            getClient().attach("runners", collectionOne).get(1, TimeUnit.SECONDS);

            updateRunner(3, "john");

            Map<String, Object> record = CollectionTraversor.findRecord("runners/id:103", collectionOne);
            //logger.info("updated record: {}", prettyPrint(record));
            assertTrue("name should have changed", record.get("name").equals("new name: 3"));
            assertTrue("status should have changed", record.get("status").equals("john"));

        } catch (InterruptedException | ExecutionException | TimeoutException | BlastException ex) {
            logger.error("error", ex);
            fail(ex.getMessage());
        }
    }

    @Test
    public void test_update_with_attachments_on_root() {
        logger.info("-- Testing Update - Simple Object Attachments");
        try {

            // create a collection
            Map<String, Object> collectionOne = new HashMap<>();

            getClient().attach("", collectionOne).get(1, TimeUnit.SECONDS);

            updateRunner(4, "smith");

            Map<String, Object> record = CollectionTraversor.findRecord("runners/id:103", collectionOne);
            //logger.info("updated record: {}", prettyPrint(record));
            assertTrue("name should have changed", record.get("name").equals("new name: 4"));
            assertTrue("status should have changed", record.get("status").equals("smith"));

        } catch (InterruptedException | ExecutionException | TimeoutException | BlastException ex) {
            logger.error("error", ex);
            fail(ex.getMessage());
        }
    }

    private void updateRunner(int nameSuffix, String status) throws BlastException, InterruptedException, ExecutionException, TimeoutException {
        RunnerDO runnerDo = createRunner(100l, 101l, 103l, "new name: " + nameSuffix, status);
        CompletableFuture<Boolean> future = getClient().update("runners/id:103", runnerDo);

        Boolean result = future.get(1, TimeUnit.SECONDS);

        // update shouls have worked
        logger.info("Record Updated: {}", result);
        assertTrue("record should have been updated", result);

    }

}

package blast.graph;

import blast.exception.BlastException;
import blast.graph.client.GraphExternalClient;
import blast.graph.core.dataobjects.Operation;
import blast.log.BlastLogger;
import blast.utils.BlastUtils;
import java.net.URI;
import java.net.URISyntaxException;
import org.junit.AfterClass;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author grant
 */
public class GraphTruePathTest {

    protected static final BlastLogger logger = BlastLogger.createLogger();

    private static GraphExternalClient theExternalClient = null;

    @BeforeClass
    public static void setup() throws BlastException {

        try {
            // we are not going to connect - so no server needed
            theExternalClient = new GraphExternalClient(new URI("ws://127.0.0.1:8080/blast"));
        } catch (URISyntaxException ex) {
            logger.error("URI format exception", ex);
            throw new BlastException("URI format exception", ex);
        }
    }

    @AfterClass
    public static void shutdown() {
        BlastUtils.sleep(1000);
    }

    @Test
    public void test_true_path_update_on_collection_1() {
        logger.info("-- Testing update on collection");
        try {

            // the key used in the attach statement - will be a list of runners
            String attachKey = "runners";

            // the path that the record was updated on
            String updatePath = "runners/id:101";

            String truePath = theExternalClient.calculateTruePath(attachKey, updatePath, Operation.UPDATE);

            logger.info("attachKey: {} updatePath: {} - calculated true path : {}", attachKey, updatePath, truePath);

            assertTrue("true path s/be", truePath.equals("runners/id:101"));
        } catch (BlastException ex) {
            logger.error("error", ex);
            fail(ex.getMessage());
        }

    }

    @Test
    public void test_true_path_update_on_root() {
        logger.info("-- Testing update on collection");
        try {

            // the key used in the attach statement - will be simple object with 'collections as elements
            String attachKey = "";

            // the path that the record was updated on
            String updatePath = "runners/id:101";

            String truePath = theExternalClient.calculateTruePath(attachKey, updatePath, Operation.UPDATE);

            logger.info("attachKey: {} updatePath: {} - calculated true path : {}", attachKey, updatePath, truePath);

            assertTrue("true path s/be", truePath.equals("runners/id:101"));
        } catch (BlastException ex) {
            logger.error("error", ex);
            fail(ex.getMessage());
        }

    }

    @Test
    public void test_true_path_update_on_collection_2() {
        logger.info("-- Testing update on collection");
        try {

            // the key used in the attach statement - will be an array
            String attachKey = "markets/id:101/runners";

            // the path that the record was updated on
            String updatePath = "markets/id:101/runners/id:103";

            String truePath = theExternalClient.calculateTruePath(attachKey, updatePath, Operation.UPDATE);

            logger.info("attachKey: {} updatePath: {} - calculated true path : {}", attachKey, updatePath, truePath);

            assertTrue("true path s/be", truePath.equals("runners/id:103"));
        } catch (BlastException ex) {
            logger.error("error", ex);
            fail(ex.getMessage());
        }

    }

    @Test
    public void test_true_path_update_on_collection_3() {
        logger.info("-- Testing update on collection");
        try {

            // the key used in the attach statement - will be single object with an element of runners
            String attachKey = "markets/id:101";

            // the path that the record was updated on
            String updatePath = "markets/id:101/runners/id:103";

            String truePath = theExternalClient.calculateTruePath(attachKey, updatePath, Operation.UPDATE);

            logger.info("attachKey: {} updatePath: {} - calculated true path : {}", attachKey, updatePath, truePath);

            assertTrue("true path s/be", truePath.equals("runners/id:103"));
        } catch (BlastException ex) {
            logger.error("error", ex);
            fail(ex.getMessage());
        }

    }

    @Test
    public void test_true_path_update_on_collection_4() {
        logger.info("-- Testing update on collection");
        try {

            // the key used in the attach statement - will be single object with an element of runners
            String attachKey = "markets";

            // the path that the record was updated on
            String updatePath = "markets/id:101/runners/id:103";

            String truePath = theExternalClient.calculateTruePath(attachKey, updatePath, Operation.UPDATE);

            logger.info("attachKey: {} updatePath: {} - calculated true path : {}", attachKey, updatePath, truePath);

            assertTrue("true path s/be", truePath.equals("markets/id:101/runners/id:103"));
        } catch (BlastException ex) {
            logger.error("error", ex);
            fail(ex.getMessage());
        }

    }

    @Test
    public void test_true_path_update_on_collection_5() {
        logger.info("-- Testing update on collection");
        try {

            // the key used in the attach statement - will be single object with an element of runners
            String attachKey = "events";

            // the path that the record was updated on
            String updatePath = "events/id:100/markets/id:101/runners/id:103";

            String truePath = theExternalClient.calculateTruePath(attachKey, updatePath, Operation.UPDATE);

            logger.info("attachKey: {} updatePath: {} - calculated true path : {}", attachKey, updatePath, truePath);

            assertTrue("true path s/be", truePath.equals("events/id:100/markets/id:101/runners/id:103"));
        } catch (BlastException ex) {
            logger.error("error", ex);
            fail(ex.getMessage());
        }

    }

    @Test
    public void test_true_path_update_on_collection_7() {
        logger.info("-- Testing update on collection");
        try {

            // the key used in the attach statement - will be single object with an element of runners
            String attachKey = "events/id:100";

            // the path that the record was updated on
            String updatePath = "events/id:100/markets/id:101/runners/id:103";

            String truePath = theExternalClient.calculateTruePath(attachKey, updatePath, Operation.UPDATE);

            logger.info("attachKey: {} updatePath: {} - calculated true path : {}", attachKey, updatePath, truePath);

            assertTrue("true path s/be", truePath.equals("markets/id:101/runners/id:103"));
        } catch (BlastException ex) {
            logger.error("error", ex);
            fail(ex.getMessage());
        }

    }

    @Test
    public void test_true_path_add_on_collection_1() {
        logger.info("-- Testing add on collection");
        try {

            // the key used in the attach statement
            String attachKey = "runners";

            // the path that the record was updated on
            String updatePath = "runners/id:999";

            String truePath = theExternalClient.calculateTruePath(attachKey, updatePath, Operation.ADD);

            logger.info("attachKey: {} updatePath: {} - calculated true path : {}", attachKey, updatePath, truePath);

            assertTrue("true path s/be", truePath.equals("runners"));
        } catch (BlastException ex) {
            logger.error("error", ex);
            fail(ex.getMessage());
        }

    }

    @Test
    public void test_true_path_add_on_collection_2() {
        logger.info("-- Testing add on collection");
        try {

            // the key used in the attach statement
            String attachKey = "events/id:100";

            // the path that the record was updated on
            String updatePath = "events/id:100/markets/id:101/runners/id:999";

            String truePath = theExternalClient.calculateTruePath(attachKey, updatePath, Operation.ADD);

            logger.info("attachKey: {} updatePath: {} - calculated true path : {}", attachKey, updatePath, truePath);

            assertTrue("true path s/be", truePath.equals("markets/id:101/runners"));
        } catch (BlastException ex) {
            logger.error("error", ex);
            fail(ex.getMessage());
        }

    }

// ---- add    
//runners/id:116
//markets/id:113/runners/id:116 true events
//markets/id:113/runners/id:116 false
//events/id:100/markets/id:113/runners/id:116 true events
//events/id:100/markets/id:113/runners/id:116 false
// ----- replace
//runners/id:103 true    runners
//runners/id:103 false    
//markets/id:101/runners/id:103 true markets
//markets/id:101/runners/id:103 false
//events/id:100/markets/id:101/runners/id:103 true events    
//events/id:100/markets/id:101/runners/id:103 false    
// --- actual - replace
// key: markets/id:101/runners/id:103    path:runners/id:103
// key:runners      path:runners/id:103
}

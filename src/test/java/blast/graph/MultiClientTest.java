package blast.graph;

import blast.Blast;
import blast.exception.BlastException;
import blast.graph.client.GraphExternalClient;
import blast.graph.core.BlastGraph;
import blast.graph.core.BlastGraphImpl;
import blast.graph.example.SportsBookGraphBuilder;
import blast.graph.example.model.RunnerDO;
import blast.graph.module.GraphModule;
import blast.log.BlastLogger;
import blast.server.BlastServer;
import blast.utils.BlastUtils;
import static blast.utils.LambdaExceptionHelper.rethrowConsumer;
import blast.vertx.engine.VertxEngine;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import org.junit.AfterClass;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author grant
 */
public class MultiClientTest {

    protected static final BlastLogger logger = BlastLogger.createLogger();

    private static BlastServer theBlastServer = null;
    private static int thePort;
    public static ObjectMapper theObjectMapper;

    @Test
    public void test_2_clients() {
        logger.info("-- Testing 2 clients");
        try {

            List<Client> clientList = new ArrayList<>();

            for (int x = 0; x < 2; x++) {
                Client client = new Client();
                clientList.add(client);

                client.connect();
                client.attachAsList("runners");
            }

            BlastUtils.safeStream(clientList).forEach(rethrowConsumer(client -> {
                logger.info("Client: (before add) data {}", client.listData.size());
                assertTrue("has data", client.getListData().size() == 30);
            }));

            BlastUtils.safeStream(clientList).forEach(rethrowConsumer(client -> {
                logger.info("Client: (before) attachments {}", client.getClient().getAttachments().get(1, TimeUnit.SECONDS));
            }));

            createRunner(clientList.get(0).getClient(), 1001l);

            BlastUtils.safeStream(clientList).forEach(rethrowConsumer(client -> {
                logger.info("Client: (before) attachments {}", client.getClient().getAttachments().get(1, TimeUnit.SECONDS));
            }));

            BlastUtils.safeStream(clientList).forEach(rethrowConsumer(client -> {
                logger.info("Client: (after add) data {}", client.listData.size());
            }));

            BlastUtils.safeStream(clientList).forEach(rethrowConsumer(client -> {
//                logger.info("Client: (after add) data {}", client.listData.size());
                assertTrue("has data", client.getListData().size() == 31);
            }));

        } catch (InterruptedException | ExecutionException | TimeoutException | BlastException ex) {
            logger.error("error", ex);
            fail(ex.getMessage());
        }
    }

    @BeforeClass
    public static void setup() {

        try {
            System.setProperty("blast-server.config", "blast-test-server.json");
            theBlastServer = startBlastServer();
            theObjectMapper = theBlastServer.getObjectMapper();

            thePort = theBlastServer.getProperties().getEndpoint().getPort();

        } catch (BlastException ex) {
            logger.error("Failed to start the server", ex);
            BlastUtils.sleep(1000);
        }
    }

    @AfterClass
    public static void shutdown() {
        if (theBlastServer != null) {
            BlastUtils.sleep(2000);
            //theBlastServer.shutdown();
        }
    }

    public static BlastServer startBlastServer() throws BlastException {
        try {

            BlastGraph blastGraph = new BlastGraphImpl();
            GraphModule graphModule = new GraphModule(blastGraph);

            BlastServer blast = Blast.blast(new VertxEngine(), new blast.module.ping.PingModule(), new blast.module.echo.EchoModule(), graphModule);

            // add server to blast graph
            blastGraph.setBlastServer(blast);

            // Build a graph repository
            SportsBookGraphBuilder graphBuilder = new SportsBookGraphBuilder(graphModule.getGraph());
            graphBuilder.buildGraph();

            logger.info("starting with properties: {}", blast.getProperties());
            blast.startup();

            // give the server a chance to start
            BlastUtils.sleep(1000);

            return blast;
        } catch (BlastException ex) {
            logger.error("Can't start Blast!", ex);
            throw ex;
        }

    }

    protected static URI getTestWsEndpoint() throws BlastException {
        try {
            return new URI("ws://127.0.0.1:" + thePort + "/blast");
            // use below to test connection errors
            // return new URI("ws://127.0.0.1:8081/blast");
        } catch (URISyntaxException ex) {
            logger.error("URI format exception", ex);
            throw new BlastException("URI format exception", ex);
        }
    }

    private class Client {

        GraphExternalClient client;
        Map<String, Object> data = new HashMap<>();
        List<Map<String, Object>> listData = new ArrayList<>();

        public Client() throws BlastException {
            client = new GraphExternalClient(getTestWsEndpoint());
        }

        public void connect() throws InterruptedException, ExecutionException, TimeoutException {
            client.connect().get(1, TimeUnit.SECONDS);
        }

        public void attach(String key) throws BlastException, InterruptedException, ExecutionException, TimeoutException {
            client.attach(key, data).get(1, TimeUnit.SECONDS);
        }

        public void attachAsList(String key) throws BlastException, InterruptedException, ExecutionException, TimeoutException {
            client.attach(key, listData).get(1, TimeUnit.SECONDS);
        }

        public Map<String, Object> getData() {
            return data;
        }

        public GraphExternalClient getClient() {
            return client;
        }

        public List<Map<String, Object>> getListData() {
            return listData;
        }
    }

    private void createRunner(GraphExternalClient client, Long runnerId) throws InterruptedException, ExecutionException, TimeoutException, BlastException {
        // add a runner
        RunnerDO runnerDo = createRunner(100l, 101l, runnerId, "new name", "open");
        CompletableFuture<Boolean> future = client.add("runners", runnerDo);

        Boolean result = future.get(1, TimeUnit.SECONDS);

//        logger.info("Record added: {}", result);

        assertTrue("record should have been added", result);

    }

    protected RunnerDO createRunner(Long eventId, Long marketId, Long id, String name, String status) {
        RunnerDO runnerDo = new RunnerDO();
        runnerDo.setId(id);
        runnerDo.setEventId(eventId);
        runnerDo.setMarketId(marketId);
        runnerDo.setName(name);
        runnerDo.setStatus(status);
        return runnerDo;
    }

}

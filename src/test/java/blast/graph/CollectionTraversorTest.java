package blast.graph;

import blast.exception.BlastException;
import blast.graph.core.utils.CollectionTraversor;
import blast.log.BlastLogger;
import blast.utils.BlastUtils;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import org.junit.AfterClass;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import org.junit.Test;

/**
 *
 * @author grant
 */
public class CollectionTraversorTest {

    protected final BlastLogger logger = BlastLogger.createLogger();
    protected final Random theRandom = new Random();

    private long barCounter = 0;

    @AfterClass
    public static void shutdown() {
        BlastUtils.sleep(1000);
    }

    @Test
    public void test_find_record_in_integer_list() {
        logger.info("-- Testing find record in collection");
        try {
            List<Map<String, Object>> testData = createIntegerTestList("id", 10);

            Map<String, Object> record = CollectionTraversor.findRecord("root/id:5", testData);
            logger.info("foo: {}", record.get("foo"));
            assertTrue("foo should match", record.get("foo").equals("foo:5"));

        } catch (BlastException ex) {
            logger.error("error", ex);
            fail(ex.getMessage());
        }

    }

    @Test
    public void test_find_record_in_string_list() {
        logger.info("-- Testing find record in collection");
        try {
            String keyName = "name";

            List<Map<String, Object>> testData = createStringTestList(keyName, 10);
            String key = (String) testData.get(8).get("name");

            Map<String, Object> record = CollectionTraversor.findRecord("root/" + keyName + ":" + key, testData);
            logger.info("foo: {}", record.get("foo"));
            assertTrue("foo should match", record.get("foo").equals("foo:" + key));

        } catch (BlastException ex) {
            logger.error("error", ex);
            fail(ex.getMessage());
        }

    }

    @Test
    public void test_find_record_in_complex_integer_list() {
        logger.info("-- Testing find record in collection");
        try {
            String keyName = "id";

            List<Map<String, Object>> testData = createComplexIntegerTestList(keyName, 10);

            Map<String, Object> record = CollectionTraversor.findRecord("root/id:5/bar/id:6", testData);
            logger.info("foo: {}", record.get("foo"));
            assertTrue("bar should match", record.get("bar").equals("bar:56"));

        } catch (BlastException ex) {
            logger.error("error", ex);
            fail(ex.getMessage());
        }

    }

    @Test
    public void test_find_list_in_complex_integer_list() {
        logger.info("-- Testing find record in collection");
        try {
            String keyName = "id";

            List<Map<String, Object>> testData = createComplexIntegerTestList(keyName, 10);

            List<Map<String, Object>> list = CollectionTraversor.findList("root/id:5/bar", testData);
            logger.info("list: {}", list);

        } catch (BlastException ex) {
            logger.error("error", ex);
            fail(ex.getMessage());
        }

    }

    @Test
    public void test_find_collections() {
        logger.info("-- Testing find record in collection");
        try {
            Map<String, Object> testData = new HashMap<>();
            testData.put("events", createComplexIntegerTestList("id", 10));
            testData.put("markets", createComplexIntegerTestList("id", 10));
            testData.put("runners", createComplexIntegerTestList("id", 10));

            Map<String, Object> record = CollectionTraversor.findRecord("runners/id:5", testData);
            logger.info("record: {}", record);

            List<Map<String, Object>> list = CollectionTraversor.findList("runners/id:5/bar", testData);
            logger.info("list: {}", list);

        } catch (BlastException ex) {
            logger.error("error", ex);
            fail(ex.getMessage());
        }

    }

    private List<Map<String, Object>> createStringTestList(String keyField, int numRecords) {
        List<Map<String, Object>> list = new ArrayList<>();
        for (int x = 0; x < numRecords; x++) {
            byte[] bytes = new byte[5];
            theRandom.nextBytes(bytes);
            list.add(createObject(keyField, String.valueOf(bytes)));
        }
        return list;
    }

    private List<Map<String, Object>> createIntegerTestList(String keyField, int numRecords) {
        List<Map<String, Object>> list = new ArrayList<>();
        for (int x = 0; x < numRecords; x++) {
            list.add(createObject(keyField, x));
        }
        return list;
    }

    private List<Map<String, Object>> createComplexIntegerTestList(String keyField, int numRecords) {
        List<Map<String, Object>> list = new ArrayList<>();
        for (int x = 0; x < numRecords; x++) {
            list.add(createComplexObject(keyField, x));
        }
        return list;
    }

    private Map<String, Object> createObject(String keyField, Object keyValue) {
        Map<String, Object> record = new HashMap<>();

        record.put(keyField, keyValue);
        record.put("foo", "foo:" + keyValue);
        record.put("bar", "bar:" + barCounter++);
        return record;
    }

    private Map<String, Object> createComplexObject(String keyField, Object keyValue) {
        Map<String, Object> record = new HashMap<>();

        record.put(keyField, keyValue);
        record.put("foo", "foo:" + keyValue);
        record.put("bar", createIntegerTestList("id", 10));
        return record;
    }
}

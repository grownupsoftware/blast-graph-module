/*
 * Grownup Software Limited.
 */
package blast.graph;

import blast.Blast;
import blast.exception.BlastException;
import blast.graph.core.BlastGraph;
import blast.graph.core.BlastGraphImpl;
import blast.graph.core.GraphCollection;
import static blast.graph.core.utils.GraphUtils.prettyPrint;
import blast.graph.core.utils.SchemaUtils;
import blast.graph.example.model.EventDO;
import blast.graph.example.model.MarketDO;
import blast.graph.example.model.RunnerDO;
import blast.graph.module.GraphModule;
import blast.log.BlastLogger;
import blast.server.BlastServer;
import blast.utils.BlastUtils;
import blast.vertx.engine.VertxEngine;
import static org.junit.Assert.fail;
import org.junit.Test;

/**
 *
 * @author grant
 */
public class GraphTest {

    protected static final BlastLogger logger = BlastLogger.createLogger();
    private long counter = 100;

    @Test
    public void test_collections() {
        try {
            logger.info("-- collection test --");
            BlastGraphImpl blastGraph = new BlastGraphImpl();

            GraphModule graphModule = new GraphModule(blastGraph);
            
            // need to create a server as blast graph uses this
            BlastServer blast = Blast.blast(new VertxEngine(), new blast.module.ping.PingModule(), new blast.module.echo.EchoModule(), graphModule);

            // add server to blast graph
            blastGraph.setBlastServer(blast);
            
            
//            logger.info("Persisted Collections: {}", getCollectionSummary(blastGraph));
//
//            blastGraph.removeCollection("events");
//            blastGraph.removeCollection("markets");
//
//            logger.info("After Remove Collections: {}", getCollectionSummary(blastGraph));
//
            // create events collection
            GraphCollection<EventDO, Long> events = blastGraph.createCollection()
                    .name("events")
                    .keyfield("id")
                    .dataclass(EventDO.class)
                    .excludeFields("markets")
                    .build();

            // create markets collection
            GraphCollection<MarketDO, Long> markets = blastGraph.createCollection()
                    .name("markets")
                    .keyfield("id")
                    .dataclass(MarketDO.class)
                    //.onlyFields("name")
                    .excludeFields("runners")
                    .build();

            // create runners collection
            GraphCollection<RunnerDO, Long> runners = blastGraph.createCollection()
                    .name("runners")
                    .keyfield("id")
                    .dataclass(RunnerDO.class)
                    //.onlyFields("name")
                    .excludeFields("prices")
                    .build();

            // event 1-* markets.eventId
            markets.linkedTo(events).using("eventId");
            runners.linkedTo(markets).using("marketId");

            logger.info("After Create Collections: {}", getCollectionSummary(blastGraph));

            // ------------------------------ create 1st event
            Long eventId = getUniqueId();
            events.add(createEvent(eventId, "event " + eventId, "open"));

            // add 1st market to event
            Long marketId = getUniqueId();
            markets.add(createMarket(marketId, eventId, "market " + marketId, "open"));

            // add 2nd market
            marketId = getUniqueId();
            markets.add(createMarket(marketId, eventId, "market " + marketId, "open"));

            // ------------------------------ create 2nd event
            eventId = getUniqueId();
            events.add(createEvent(eventId, "event " + eventId, "open"));

            // add 1st market
            marketId = getUniqueId();
            markets.add(createMarket(marketId, eventId, "market " + marketId, "open"));

            // add runner to market
            Long runnerId = getUniqueId();
            RunnerDO lastRunnerDo = createRunner(runnerId, eventId, marketId, "runner " + runnerId, "open");
            runners.add(lastRunnerDo);

//            createData(1, 1, 1, events, markets, runners);
            logger.info("Schema: {}", SchemaUtils.dumpSchema(blastGraph.getGraph()));

            logger.info("Root Data: {}", prettyPrint(blastGraph.getRootData(null)));

            logger.info("Data key: {}: Data:{}", 100l, prettyPrint(blastGraph.getData(100l, null)));
            logger.info("Data Collection: {}: Data:{}", "events", prettyPrint(blastGraph.getCollectionData("events", null)));

            blastGraph.bubbleUp(102l);

            // lets change a value
            lastRunnerDo.setStatus("closed");
            runners.update(lastRunnerDo);

            //logger.info("==> graph: {}", GraphUtils.logGraph(blastGraph.getGraph()));
        } catch (Throwable ex) {
            logger.error("error", ex);
            fail(ex.getMessage());
        } finally {
            BlastUtils.sleep(1000);
        }

    }

    private void createData(int numberEvents, int numMarketsPerEvent, int numRunnersPerMarket, GraphCollection eventCollection, GraphCollection marketCollection, GraphCollection runnerCollection) throws BlastException {

        for (int eventCnt = 0; eventCnt < numberEvents; eventCnt++) {
            Long eventId = getUniqueId();
            eventCollection.add(createEvent(eventId, "event " + eventId, "open"));
            for (int marketCnt = 0; marketCnt < numMarketsPerEvent; marketCnt++) {
                Long marketId = getUniqueId();
                marketCollection.add(createMarket(marketId, eventId, "market " + marketId, "open"));
                for (int runnerCnt = 0; runnerCnt < numRunnersPerMarket; runnerCnt++) {
                    Long runnerId = getUniqueId();
                    runnerCollection.add(createRunner(runnerId, eventId, marketId, "runner " + runnerId, "open"));
                }
            }
        }
    }

    private Long getUniqueId() {
        return counter++;
    }

    private String getCollectionSummary(BlastGraphImpl blastGraph) throws BlastException {
        StringBuilder builder = new StringBuilder();
        builder.append("\n").append("Collections:").append("\n");
        BlastUtils.safeStream(blastGraph.getCollections()).forEach(collection -> {
            builder.append("\t").append(collection.toString()).append("\n");
        });
        return builder.toString();
    }

    private EventDO createEvent(Long id, String name, String status) {
        EventDO eventDo = new EventDO();
        eventDo.setId(id);
        eventDo.setName(name);
        eventDo.setStatus(status);
        return eventDo;
    }

    private MarketDO createMarket(Long id, Long eventId, String name, String status) {
        MarketDO marketDo = new MarketDO();
        marketDo.setId(id);
        marketDo.setEventId(eventId);
        marketDo.setName(name);
        marketDo.setStatus(status);
        return marketDo;
    }

    private RunnerDO createRunner(Long id, Long eventId, Long marketId, String name, String status) {
        RunnerDO runnerDo = new RunnerDO();
        runnerDo.setId(id);
        runnerDo.setEventId(eventId);
        runnerDo.setMarketId(marketId);
        runnerDo.setName(name);
        runnerDo.setStatus(status);
        return runnerDo;
    }

}

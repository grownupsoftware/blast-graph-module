package blast.graph;

import blast.exception.BlastException;
import static blast.graph.AbstractGraphTest.logger;
import blast.graph.core.dataobjects.PathParameters;
import static blast.graph.core.utils.GraphUtils.prettyPrint;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import org.junit.Ignore;
import org.junit.Test;

/**
 *
 * @author grant
 */
public class GraphParamTest extends AbstractGraphTest {

    @Test
    @Ignore
    public void test_simple_fetch_as_list_with_params_root() {
        logger.info("-- Testing Fetch events with params");
        try {
            PathParameters pathParameters = new PathParameters();
            pathParameters.setIncludeChildren(false);

            Map<String, Object> data = getClient().fetch("", pathParameters).get(1, TimeUnit.SECONDS);
            logger.info("data: {}", prettyPrint(data));
            assertTrue("data should not be empty", !data.isEmpty());

        } catch (InterruptedException | ExecutionException | TimeoutException | BlastException ex) {
            logger.error("error", ex);
            fail(ex.getMessage());
        }
    }

    @Test
    @Ignore
    public void test_simple_fetch_as_list_with_params_events() {
        logger.info("-- Testing Fetch events with params");
        try {
            PathParameters pathParameters = new PathParameters();
            pathParameters.setIncludeChildren(false);

            List<Map<String, Object>> listData = getClient().fetchAsList("events", pathParameters).get(1, TimeUnit.SECONDS);
            logger.info("list data: {}", prettyPrint(listData));
            assertTrue("list data should not be empty", !listData.isEmpty());

            Map<String, Object> data = getClient().fetch("events/id:100", pathParameters).get(1, TimeUnit.SECONDS);
            logger.info("data: {}", prettyPrint(data));
            assertTrue("data should not be empty", !data.isEmpty());

        } catch (InterruptedException | ExecutionException | TimeoutException | BlastException ex) {
            logger.error("error", ex);
            fail(ex.getMessage());
        }
    }

    @Test
    @Ignore
    public void test_simple_fetch_as_list_with_params_markets() {
        logger.info("-- Testing Fetch markets with params");
        try {
            PathParameters pathParameters = new PathParameters();
            pathParameters.setIncludeChildren(false);

            List<Map<String, Object>> listData = getClient().fetchAsList("events/id:100/markets", pathParameters).get(1, TimeUnit.SECONDS);
            logger.info("list data: {}", prettyPrint(listData));
            assertTrue("list data should not be empty", !listData.isEmpty());

            Map<String, Object> data = getClient().fetch("events/id:100/markets/id:101", pathParameters).get(1, TimeUnit.SECONDS);
            logger.info("data: {}", prettyPrint(data));
            assertTrue("data should not be empty", !data.isEmpty());

        } catch (InterruptedException | ExecutionException | TimeoutException | BlastException ex) {
            logger.error("error", ex);
            fail(ex.getMessage());
        }
    }

    @Test
    @Ignore
    public void test_simple_fetch_as_list_with_params_events_with_fields() {
        logger.info("-- Testing Fetch events with params");
        try {
            PathParameters pathParameters = new PathParameters();
            pathParameters.setIncludeChildren(false);
            pathParameters.setFields(Arrays.asList(new String[]{"status"}));

            List<Map<String, Object>> listData = getClient().fetchAsList("events", pathParameters).get(1, TimeUnit.SECONDS);
            logger.info("list data: {}", prettyPrint(listData));
            assertTrue("list data should not be empty", !listData.isEmpty());

            Map<String, Object> data = getClient().fetch("events/id:100", pathParameters).get(1, TimeUnit.SECONDS);
            logger.info("data: {}", prettyPrint(data));
            assertTrue("data should not be empty", !data.isEmpty());

        } catch (InterruptedException | ExecutionException | TimeoutException | BlastException ex) {
            logger.error("error", ex);
            fail(ex.getMessage());
        }
    }

    @Test
    @Ignore
    public void test_simple_fetch_as_list_with_params_markets_with_fields() {
        logger.info("-- Testing Fetch events with params");
        try {
            PathParameters pathParameters = new PathParameters();
            pathParameters.setIncludeChildren(false);
            pathParameters.setFields(Arrays.asList(new String[]{"name"}));

            List<Map<String, Object>> listData = getClient().fetchAsList("events/id:100/markets", pathParameters).get(1, TimeUnit.SECONDS);
            logger.info("list data: {}", prettyPrint(listData));
            assertTrue("list data should not be empty", !listData.isEmpty());

            Map<String, Object> data = getClient().fetch("events/id:100/markets/id:101", pathParameters).get(1, TimeUnit.SECONDS);
            logger.info("data: {}", prettyPrint(data));
            assertTrue("data should not be empty", !data.isEmpty());

        } catch (InterruptedException | ExecutionException | TimeoutException | BlastException ex) {
            logger.error("error", ex);
            fail(ex.getMessage());
        }
    }

    @Test
    public void test_simple_attach_as_list_with_params_events_with_fields() {
        logger.info("-- Testing Fetch events with params");
        try {
            PathParameters pathParameters = new PathParameters();
            pathParameters.setIncludeChildren(false);
            pathParameters.setFields(Arrays.asList(new String[]{"status"}));

            List<Map<String, Object>> listData = new ArrayList<>();
            getClient().attach("events", listData, pathParameters).get(1, TimeUnit.SECONDS);
            logger.info("list data: {}", prettyPrint(listData));
            assertTrue("list data should not be empty", !listData.isEmpty());

            Map<String, Object> data = new HashMap<>();
            getClient().attach("events/id:100", data, pathParameters).get(1, TimeUnit.SECONDS);
            logger.info("data: {}", prettyPrint(data));
            assertTrue("data should not be empty", !data.isEmpty());

        } catch (InterruptedException | ExecutionException | TimeoutException | BlastException ex) {
            logger.error("error", ex);
            fail(ex.getMessage());
        }
    }

}
